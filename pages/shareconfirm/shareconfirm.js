var dataManage = require("../../datamanage/apidatamanage.js");
var util = require('../../utils/util.js');

Page({
    data: {
        id: 0,
        data:{}
    },
    onLoad: function (options) {
        this.setData({
            id: options.id || 3
        });
        this.loadData();
        this.loadShareToken();
    },
    loadShareToken:function(){

      var that = this
      dataManage.requestDataWithAPI('service/gen-token', 'GET', null, true, res => {
        that.setData({
          share_token: res.data.share_token
        })
      })

    },
    onReady: function () {
        // Do something when page ready.
    },
    onShow: function () {
        // Do something when page show.
    },
    onHide: function () {
        // Do something when page hide.
    },
    onUnload: function () {
        // Do something when page close.
    },
    onPullDownRefresh: function () {
        // Do something when pull down.
    },
    onReachBottom: function () {
        // Do something when page reach bottom.
    },
    onShareAppMessage: function () {
      return {
        title: this.data.data.title,
        imageUrl: this.data.data.thumbnail,
        path: `/pages/detail/detail?url=${this.data.data.link}&share_token=${this.data.share_token}`
      } 
    },
    onPageScroll: function () {
        // Do something when page scroll
    },

    loadData: function () {
        dataManage.requestDataWithAPI(`news/detail/${this.data.id}`, "GET", null, false, (res) => {
            if (res.code == 0) {
                this.setData({
                    data: res.data
                })
            }
        }, null, () => {
            util.hideLoadingDialog();
        });
    },
})