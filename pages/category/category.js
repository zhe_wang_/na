let app = getApp();

var dataManage = require("../../datamanage/apidatamanage.js");
var util = require('../../utils/util.js');
Page({
    data: {
        category: [],
        expandItems:{},
        subCategory:{},
        thirdCategory: {},
        carts: 0,
        reply_me: 0,
        showImage: false,
        image: "",
        currentCateIndex: 0,
        // scrollLeft: 0,
        windowHeight: app.globalData.windowHeight,
        contentHeight: app.globalData.windowHeight - 390 / 750 * app.globalData.windowWidth,
        verticalMenu:[
            {
                type: 'user',
                icon: '../../images/icon-user.png',
                count: 0
            },
            {
                type: 'cart',
                icon: '../../images/icon-cart.png',
                count: 0
            },
            // {
            //     type: 'ask',
            //     icon: '../../images/icon-ask.png',
            //     count: 0,
            //     text:'在线客服'
            // },
            {
                type: 'add',
                icon: '../../images/icon-add.png',
                count: 0
            },
            {
                type: 'at',
                icon: '../../images/icon-at.png',
                count: 0
            },
        ],
        scrollToCateView: ''
    },
  onLoad: function () {
    //首次加载显示红点，点击之后红点消失。
    wx.setStorage({
      key: 'show_point',
      data: false
    })
    this.getUserCounts(function(){});
    this.loadData();

  },
  onLeftMenuClicked: function (e){
    let type = e.currentTarget.dataset.type;
    if(type == 'user'){
      wx.navigateTo({
        url: '../usercenter/usercenter',
      });
    } else if (type == 'cart') {
        wx.navigateTo({
            url: `/pages/cart/cart?status=6`,
        });
    } else if (type == 'at'){
      this.showMsg()
    }
    else if (type == 'add') {
      this.showFavorite()
    }
  },
  showFavorite: function () {
    wx.navigateTo({
      url: '/pages/myfavorite/myfavorite'
    })
  },
  showMsg: function (e) {
    wx.navigateTo({
      url: '/pages/mymessages/mymessages'
    })
  },
  onShowCart: function (e) {
    wx.navigateTo({
      url: `/pages/cart/cart?status=6`
    })
  },
  getUserCounts: function (cb) {
    var context = this;
    dataManage.requestDataWithAPI("/user/stats", "POST", null, true, function (res) {
      if (res.code == 0) {

        var carts = res.data.carts;
        var reply_me = res.data.reply_me
        context.data.verticalMenu[1].count = carts;
        context.data.verticalMenu[3].count = reply_me;
        
        context.setData({
          verticalMenu: context.data.verticalMenu
        })
        
      }
    }, function (res) {
    }, function () {

    });
  },
  loadData: function () {
    var context = this;
    context.showWaitDialog();
    dataManage.requestDataWithAPI("category/tree", "GET", null, false, function (res) {
      if (res.code == 0) {
        var data = res.data.data;
        context.setData({
          category: res.data.category,
          subCategory: res.data.subCategory,
          thirdCategory: res.data.thirdCategory,
        });

        var currentIndex = wx.getStorageSync('currentIndex')
        if (currentIndex) {
          // Do something with return value
        } else {
          currentIndex = "0"
        }
        context.setData({
          currentCateIndex: currentIndex,
        });
      }
    }, null, function () {
      context.hideWaitDialog();
      context.setData({
        isRefresh: false,
        showLoadMoreLoading: false
      });

      context.intialCate();

    });
  },

  showWaitDialog: function () {
    util.showLoadingDialog("加载中...", 10000);

  },

  hideWaitDialog: function () {
    wx.hideNavigationBarLoading();
    wx.stopPullDownRefresh();
    wx.hideToast();
  },
    onReady: function () {
        // Do something when page ready.
    },
    onShow: function () {
        // Do something when page show.
    },
    onHide: function () {
        // Do something when page hide.
    },
    onUnload: function () {
        // Do something when page close.
    },
    onPullDownRefresh: function () {
        // Do something when pull down.
    },
    onReachBottom: function () {
        // Do something when page reach bottom.
    },
    //onShareAppMessage: function () {
    // return custom share data when user share.
    //},
    onPageScroll: function () {
        // Do something when page scroll
    },
    onScrollViewTouch: function (e) {

    },

  intialCate: function (e) {
    var currentIndex = this.data.currentCateIndex
    
    wx.createSelectorQuery().select(`#cate_${this.data.category[currentIndex].id}`).fields({
      size: true,
    }, (res) => {
      let scrollDis = 0;
        if (this.data.currentCateIndex < this.data.category.length - 1) {
          wx.createSelectorQuery().select(`#cate_${this.data.category[this.data.currentCateIndex].id}`).fields({
            size: true
          }, (resNew) => {
            // scrollDis = this.data.scrollLeft + (res.width + resNew.width) / 2;
            // console.error(this.data.scrollLeft);
            // console.error(res.width);
            // console.error(resNew.width);
            // console.error(scrollDis);
            this.setData({
              currentCateIndex: this.data.currentCateIndex,
              // scrollLeft: scrollDis,
              expandItems: {}
            })
          }).exec();

        } else {
          return;
        }
      

    }).exec();
  },
    onSwitchBtnClicked: function (e) {
        let type = e.currentTarget.dataset.type;
        let currentIndex = this.data.currentCateIndex;
        wx.createSelectorQuery().select(`#cate_${this.data.category[currentIndex].id}`).fields({
            size: true,
        }, (res)=> {
            let scrollDis = 0;
            if('right' === type) {
                if(this.data.currentCateIndex < this.data.category.length - 1) {
                    this.data.currentCateIndex++;
                    wx.createSelectorQuery().select(`#cate_${this.data.category[this.data.currentCateIndex].id}`).fields({
                        size: true
                    }, (resNew)=>{
                        // scrollDis = this.data.scrollLeft + (res.width + resNew.width) / 2;
                      // console.error(this.data.scrollLeft);
                      // console.error(res.width);
                      // console.error(resNew.width);
                      // console.error(scrollDis);
                        this.setData({
                            currentCateIndex: this.data.currentCateIndex,
                            // scrollLeft: scrollDis,
                            expandItems:{}
                        })
                    }).exec();

                } else {
                    return;
                }
            } else {
                if(this.data.currentCateIndex > 0 && this.data.currentCateIndex < this.data.category.length) {
                    this.data.currentCateIndex--;
                    wx.createSelectorQuery().select(`#cate_${this.data.category[this.data.currentCateIndex].id}`).fields({
                        size: true
                    }, (resNew)=>{
                        // scrollDis = this.data.scrollLeft - (res.width + resNew.width) / 2;
                      // console.error(this.data.scrollLeft);
                      // console.error(res.width);
                      // console.error(resNew.width);
                      // console.error(scrollDis);
                        this.setData({
                            currentCateIndex: this.data.currentCateIndex,
                            // scrollLeft: scrollDis,
                            expandItems:{}
                        })

                    }).exec();
                } else {
                    return;
                }
            }

        }).exec();


    },
    onCloseClicked: function (e) {

      let currentIndex = this.data.currentCateIndex;
      console.log("category_id:" + this.data.category[currentIndex].id);
      console.log("category_id:" + this.data.category[currentIndex].name);

      wx.setStorage({
        key: "category_id",
        data: this.data.category[currentIndex].id
      })
      wx.setStorage({
        key: "category_name",
        data: this.data.category[currentIndex].name
      })
      wx.setStorage({
        key: "currentIndex",
        data: this.data.currentCateIndex
      })

        wx.reLaunch({
          url: '../index/index',
        })();
    },
    toggleThirdCate: function (e) {
      let subCateId = e.currentTarget.dataset.subCateId;

      this.data.expandItems[subCateId] = this.data.expandItems[subCateId] == 1 ? 0 : 1;
      this.setData({
        expandItems: this.data.expandItems
      })
    },
    onSubCateClicked: function (e) {
      let subCateId = e.currentTarget.dataset.item;
      this.data.expandItems[subCateId] = this.data.expandItems[subCateId] == 1 ? 0 : 1;
      if (this.data.expandItems[subCateId] == 1) {
        this.setData({
          scrollToCateView: `cate_${subCateId}`,
          expandItems: this.data.expandItems
        })
      } else {
        this.setData({
          scrollToCateView: '',
          expandItems: this.data.expandItems
        })
      }
      //   let item = e.currentTarget.dataset.item;
      // wx.navigateTo({
      //   url: '../index/index?id={0}&initialMenuId={1}'.format(item.parent_id, item.id)
      // });
    },
    onThirdCateClicked: function (e) {

      let item = e.currentTarget.dataset.item;

      if (!item.image_path){
        //要延时执行的代码  
        wx.reLaunch({
          url: '../index/index?id={0}&initialMenuId={1}'.format(item.parent_id, item.id)
                  });
      }else{
      this.setData({
        showImage: true,
        image: item.image_path
      })

      var that = this
      setTimeout(function () {
        //要延时执行的代码  
        wx.reLaunch({
          url: '../index/index?id={0}&initialMenuId={1}'.format(item.parent_id, item.id),
          complete: function () {
            that.setData({
              showImage: false
            })
          }
        });
      }, 2000) //延迟时间 这里是1秒  
      }
    }
})