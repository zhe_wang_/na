var makeurl = require("../../utils/urls.js");
var dataManage = require("../../datamanage/apidatamanage.js");
var util = require('../../utils/util.js');
// pages/comment/comment.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        news_id: 0,
        media_id: [],
        //src 需要上传的文件
        src: [],
        //语音文件
        audio_src: '',
        media_voice_id: '',
        media_video_id: '',
        //thumb缩略图
        thumb: [],
        currentIndex: 0,
      thumb_id: '',
      showAddBtn: true,
        srctype: '',
        inputValue: '',
        audio_playing: false,
        indicatorDots: true,
        autoplay: true,
        audioRecording: false,
        interval: 5000,
        duration: 1000,
        avatar: '',
        uname: '',
        cates:[
            {
                name: '性别',
                type: 'radio',
                value: '',
                selectValues: [
                    '男', '女'
                ]
            },
            {
                name: '年龄',
                type: 'date',
                value: ''
            },
            {
                name: '身高',
                type: 'input',
                value: ''
            },
            {
                name: '信仰',
                type: 'radio',
                value: '',
                selectValues:[
                    '信仰1', '信仰2', '信仰3'
                ]
            },
            {
                name: '教育',
                type: 'input',
                value: ''
            },
            {
                name: '职业',
                type: 'input',
                value: ''
            },
            {
                name: '婚姻状态',
                type: 'radio',
                value: '',
                selectValues:[
                    '已婚', '未婚', '离异'
                ]
            },
            {
                name: '现居住地',
                type: 'input',
                value: ''
            },
            {
                name: '是否吸烟',
                type: 'radio',
                value: '',
                selectValues:[
                    '是','否'
                ]
            },
        ],
        showCateDialog: false,
        currentSelectedCateIndex: -1,
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            news_id: options.news_id || 12,
            uname: wx.getStorageSync('uname'),
            avatar: wx.getStorageSync('avatar')
        });

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        if (this.innerAudioContext) {
            this.innerAudioContext.stop();
            this.innerAudioContext.destroy();
        }
        if(this.recorderManager) {
            this.recorderManager.onStart(null);
            this.recorderManager.onPause(null);
            this.recorderManager.onStop(null);
            this.recorderManager.onError(null);
        }

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },

    bindKeyInput: function (e) {
        this.setData({
            inputValue: e.detail.value
        })
    },
    getVideo: function () {
        var that = this;
        wx.chooseVideo({
            sourceType: ['album', 'camera'],
            maxDuration: 15,
            camera: 'back',
            success: function (res) {
                // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
                console.error(res)
                that.setData({
                    src: [res.tempFilePath],
                    thumb: [res.thumbTempFilePath],
                    srctype: 'video'
                });
            }
        })
    },
    deleteMedia: function () {
        console.error(this.data.currentIndex)
        var that = this;
        var thumb = this.data.thumb;

        var src = this.data.src;
        wx.showModal({
            title: '提示',
            content: '确定要删除吗？',
            success: function (res) {
                if (res.confirm) {
                    src.splice(that.data.currentIndex, 1);

                    thumb.splice(that.data.currentIndex, 1);
                    that.setData({
                        thumb,
                        src
                    });
                } else if (res.cancel) {
                    return false;
                }

            }
        })

        console.error(this.data.thumb)
    },

  resetAddBtn: function () {
    this.setData({
      showAddBtn: this.data.currentIndex == this.data.thumb.length
    })
  },
  imageIndexChanged: function (e) {
    this.setData({
      currentIndex: e.detail.current
    });
    this.resetAddBtn();
  },


    getAudio1: function () {
        var that = this
        wx.showModal({
            title: '提示',
            content: '正在录音',
            success: function (res) {
                if (res.confirm) {
                    wx.stopRecord()
                } else if (res.cancel) {
                    recorderManager.stop()
                }
            }
        })
        wx.startRecord({
            success(res) {
                that.setData({
                    audio_playing: false,
                    audio_src: res.tempFilePath,
                })
            }
        });
        setTimeout(function () {
            wx.stopRecord() // 结束录音
        }, 10000)
    },
    getAudio: function () {
        var that = this;
        const recorderManager = wx.getRecorderManager()
        const options = {
            duration: 60,
            sampleRate: 44100,
            numberOfChannels: 1,
            encodeBitRate: 192000,
            format: 'mp3',
            frameSize: 50
        };
        recorderManager.onError((res) => {
            // 录音失败的回调处理
            console.error(res);
        });
        recorderManager.onStart(() => {
            console.log('recorder start')
            wx.showModal({
                title: '提示',
                content: '正在录音',
                success: function (res) {
                    if (res.confirm) {
                        recorderManager.stop()
                    } else if (res.cancel) {
                        recorderManager.stop()
                    }
                }
            })
        })
        recorderManager.onStop((res) => {
            console.log('recorder stop', res)
            that.setData({
                audio_src: res.tempFilePath,
            });
        })
        recorderManager.start(options)

    },
    deleteAudio: function () {
        this.setData({
            audio_playing: false,
            audio_src: '',
            media_voice_id: ''
        })
    },

    recordAudio: function (e) {
        if(this.data.audioRecording) {
            this.recorderManager.stop();
            return;
        }
        if(!this.recorderManager) {
            this.recorderManager = wx.getRecorderManager();
        }
        this.recorderManager.onStart(() => {
            this.setData({
                audioRecording: true
            });
            console.log('recorder start');
        });
        this.recorderManager.onPause(() => {
            console.log('recorder pause');
        });
        this.recorderManager.onStop((res) => {
            console.log('recorder stop', res);
            this.setData({
                audioRecording: false
            });
            this.data.audio_src = res.tempFilePath;
        });
        this.recorderManager.onError(res=>{
            console.log('recorder error' + res);
        });

        let options = {
            duration: 10000,
            sampleRate: 44100,
            numberOfChannels: 1,
            encodeBitRate: 192000,
            format: 'mp3',
            frameSize: 50
        };

        this.recorderManager.start(options)

    },
    playAudio: function () {

        if (!this.data.audio_src) {
            return
        }
        this.innerAudioContext = wx.createInnerAudioContext();
        this.innerAudioContext.onError((res) => {
            // 播放音频失败的回调
            console.error(res);
        });
        this.innerAudioContext.src = this.data.audio_src; // 这里可以是录音的临时路径
        this.innerAudioContext.play()
    },
    getImg: function () {

        var that = this;
        wx.chooseImage({
            count: 6, // 默认9
            sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
            sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
            success: function (res) {
                that.setData({
                    src: res.tempFilePaths,
                    thumb: res.tempFilePaths,
                    srctype: 'image'
                });
            }
        })
    },
    formSubmit: function (e) {
      if (!this.data.inputValue || this.data.inputValue.length <= 0) {
        wx.showToast({
          title: '请输入发布内容',
          icon: 'none',
          duration: 1000
        })
        return;
      }

        var that = this;
        wx.showLoading({
            title: '提交中',
        });

        let fileArr = [];
        if(this.data.src.length > 0) {
            for(let i = 0; i < this.data.src.length; i++) {
                let item = {path: this.data.src[i], type: this.data.srctype};
                fileArr.push(item);
            }
        }
        if(this.data.audio_src && this.data.audio_src.length > 0) {
            fileArr.push({path: this.data.audio_src, type: "audio"});
        }
        this.data.media_id = [];

        if(fileArr.length > 0) {
            dataManage.uploadFiles(fileArr, res=>{
                console.error("UPLOAD_SUCCESS");
                console.error(res);
                for(let i = 0; i< res.length; i++) {
                    let item = res[i];
                    if(item.type === 'image') {
                        this.data.media_id.push(item.id);
                    } else if(item.type === 'audio') {
                        this.data.media_voice_id = item.id;
                    } else if(item.type === 'video') {
                        this.data.media_video_id = item.id;
                    }
                }
                this.postComment();
            }, err=>{
                console.error("UPLOAD_ERROR");
                console.error(err);
                wx.hideLoading();
            })
        } else {
            this.postComment();
        }
    },
    formReset: function () {
        console.log('form发生了reset事件')
    },

    uploadVoide: function () {
        if (this.data.audio_src) {
            wx.uploadFile({
                url: makeurl.makeAPIUrls('fileupload'),
                filePath: this.data.audio_src,
                name: 'upfile',
                method: 'POST',
                formData: {
                    'type': 'audio'
                },
                header: {
                    'news-access-token': wx.getStorageSync('access_token')
                },
                success: function (res) {
                    var id = JSON.parse(res.data).data.id
                    that.setData({
                        media_audio_id: id,
                    });
                    that.postComment();
                },
                fail: function () {
                    wx.hideLoading();
                    wx.showToast({
                        title: '发布失败',
                        icon: 'none',
                        duration: 1000
                    })
                },
                complete: function () {
                }
            })
        } else {
            that.postComment();
        }
    },
    postComment: function () {
        let tags = [];
        for(let i = 0; i < this.data.cates.length; i++) {
            let item = this.data.cates[i];
            if(!util.isEmpty(item.value)) {
                tags.push(`${item.name}:${item.value}`);
            }
        }
        let params = {
            text: this.data.inputValue,
            news_id: this.data.news_id,
            media_id: this.data.media_id.toString(),
            media_voice_id: this.data.media_voice_id,
            media_video_id: this.data.media_video_id,
        };
        if(tags.length > 0) {
            params['tags'] = tags.join('(#)');
        }

        dataManage.requestDataWithAPI("/news/contect", "POST", params, true,
            function (res) {
                wx.showToast({
                    title: '发布成功',
                    icon: 'success',
                    duration: 1000,
                    complete: function () {
                        setTimeout(function () {
                            //要延时执行的代码
                            wx.navigateBack()
                        }, 1000)
                    }
                })
            },
            function (res) {

                wx.showToast({
                    title: '发布失败',
                    icon: 'none',
                    duration: 1000
                })
            },
            function () {
                wx.hideLoading();
            });
    },
    onCateItemClicked: function (e) {
        let index = e.currentTarget.dataset.index;
        let item = e.currentTarget.dataset.item;
        if(item.type === 'input') {
            this.setData({
                currentSelectedCateIndex: index,
                showCateDialog: true
            });
        } else if(item.type === 'radio') {
            wx.showActionSheet({
              itemList: item.selectValues,
              success:res => {
                  this.data.cates[index].value = this.data.cates[index]['selectValues'][res.tapIndex];
                  this.setData({
                      currentSelectedCateIndex: index,
                      cates: this.data.cates
                  })
              }
            })
        }

    },
    cateInput: function (e) {
        console.error(e);
        let value = e.detail.value;
        this.data.cates[this.data.currentSelectedCateIndex]['tempValue'] = value;
    },
    onCancelClicked: function (e) {
        this.data.cates[this.data.currentSelectedCateIndex]['tempValue'] = '';
        this.setData({
            cates: this.data.cates,
            showCateDialog: false
        });
    },
    onConfirmClicked: function () {
        this.data.cates[this.data.currentSelectedCateIndex].value = this.data.cates[this.data.currentSelectedCateIndex]['tempValue'];
        this.setData({
            cates: this.data.cates,
            showCateDialog: false
        });
    },
    bindDateChange:function (e) {
        let index = e.currentTarget.dataset.index;
        let item = e.currentTarget.dataset.item;
        let date = e.detail.value;
        this.data.cates[index].value = date;
        this.setData({
            cates: this.data.cates,
        })
    },

    

})