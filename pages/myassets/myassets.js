let makeurl = require("../../utils/urls.js");
let dataManage = require("../../datamanage/apidatamanage.js");
let util = require('../../utils/util.js');
let evenBus = require('../../utils/event-bus');
let constant = require('../../utils/constant');
Page({
    data: {
        menu:[

          {
            id: 3,
            name: '积分',
            count: 0
          },
            {
             id: 1,
            name: '股权',
            count: 0
            },
            {
              id: 2,
              name: '票券',
              count: 0
            },
        ],
        currentSelectedIndex: 0,
        tickets:{
            page:0,
            hasMore: false,
            list:[]
        },
        stocks:{
            page:0,
            hasMore: false,
            list:[]
        },
        credits:{
            step: 0,
            account: '',
            credit: '',
            creditLog:[],
            loadedData: false,
            transferUser:{},
            count: '',
        }
    },
    onLoad: function (options) {
        this.loadData(3, 1);
        evenBus.on(constant.TAG_CREDIT_DETACHED, this, data=>{
            let credits = data.data;
            this.data.credits.step = credits.step;
            this.data.credits.account = credits.account;
            this.data.credits.transferUser = credits.transferUser;
            this.data.credits.count = credits.count;
            this.data.credits.transferring = credits.transferring;
            this.setData({
                credits: credits
            });
        });
    },
    onReady: function () {
        // Do something when page ready.
    },
    onShow: function () {
        // Do something when page show.
    },
    onHide: function () {
        // Do something when page hide.
    },
    onUnload: function () {
        evenBus.remove(constant.TAG_CREDIT_DETACHED, this);
    },
    onPullDownRefresh: function () {
        // Do something when pull down.
    },
    onReachBottom: function () {
        // Do something when page reach bottom.
    },
    //onShareAppMessage: function () {
    // return custom share data when user share.
    //},
    onPageScroll: function () {
        // Do something when page scroll
    },
    onMenuItemClicked: function (e) {
        let item = e.currentTarget.dataset.item;
        let index = e.currentTarget.dataset.index;
        if(index == this.data.currentSelectedIndex) {
            return;
        } else {
            if (index == 0 && this.data.tickets.list.length <= 0) {
                this.loadData(item.id, 1);
            } else if (index == 1 && this.data.stocks.list.length <= 0) {
                this.loadData(item.id, 1);
            } else if (index == 2  && !this.data.credits.loadedData) {
                this.loadData(item.id, 1);
            }
            this.setData({
                currentSelectedIndex: index
            })
        }
    },
  onSwiperChanged: function (e) {
    let index = e.detail.current;
    if (index === this.data.currentSelectedIndex) {
      return;
    }
    this.onMenuItemChanged(index);
  },

    onMenuItemChanged: function (index) {
        let item = this.data.menu[index];
        this.setData({
            currentSelectedIndex: index
        });
        if (index == 0 && this.data.tickets.list.length <= 0) {
            this.loadData(item.id, 1);
        } else if (index == 1 && this.data.stocks.list.length <= 0) {
            this.loadData(item.id, 1);
        } else if (index == 2  && !this.data.credits.loadedData) {
            this.loadData(item.id, 1);
        }

    },
    loadData: function (id, page) {
        if(id === 1) {//股权
            this.loadStocks(page);
        } else if(id === 2) { //票券
            this.loadTickets(page);
        } else if(id === 3) { //积分
            this.loadCredits();
        }
    },
    loadTickets: function (page) {
        dataManage.requestDataWithAPI('asset/list', 'GET', {page: page, type: 2}, true, res=>{
            let {current_page, has_next_page, data} = res.data;
            this.data.tickets.page = current_page;
            this.data.tickets.hasMore = has_next_page;
            this.data.tickets.list = page === 1 ? data : [...this.data.tickets.list, ...data];
            this.setData({
                tickets: this.data.tickets
            });
            if(current_page == 1) {
                wx.pageScrollTo({
                    scrollTop: 0,
                    duration: 1
                });
            }
        });
    },
    loadStocks: function (page) {
        dataManage.requestDataWithAPI('asset/list', 'GET', {page: page, type: 1}, true, res=>{
            let {current_page, has_next_page, data} = res.data;
            this.data.stocks.page = current_page;
            this.data.stocks.hasMore = has_next_page;
            this.data.stocks.list = page === 1 ? data : [...this.data.stocks.list, ...data];
            this.setData({
                stocks: this.data.stocks
            });
            if(current_page == 1) {
                wx.pageScrollTo({
                    scrollTop: 0,
                    duration: 1
                });
            }
        });
    },
    loadCredits: function () {
        dataManage.requestDataWithAPI('jifen/my-jifen', 'GET', {}, true, res => {
            let {jifen, jifen_log} = res.data;
            this.data.credits.credit = jifen;
            this.data.credits.creditLog = jifen_log;
            this.data.credits.loadedData = true;
            this.setData({
                credits: this.data.credits
            });
            wx.pageScrollTo({
                scrollTop: 0,
                duration: 1
            });
        });
    },
    onScrollTop: function(e) {
        this.loadData(this.data.menu[this.data.currentSelectedIndex].id);
    },
    onScrollBottom: function(e) {
        let id = this.data.menu[this.data.currentSelectedIndex].id;
        if (id == 3) {
            return;
        }
        let assets = this.data.tickets;
        if (id == 1) {
            assets = this.data.stocks;
        }
        if (assets.list.length > 0 && assets.hasMore) {
            this.loadData(id, assets.currentPage + 1);
        }
    },
  onMoreClicked: function (e) {
    wx.navigateTo({
      url: '/pages/category/category'
    })
  },
})