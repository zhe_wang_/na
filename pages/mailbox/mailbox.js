// pages/mailbox/mailbox.js
var url = require("../../utils/urls.js");
var dataManage = require("../../datamanage/apidatamanage.js");
var util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
    this.loadData(1)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  loadData: function (page) {
    this.showWaitDialog();
    var context = this;
    var params = { page: page};
    dataManage.requestDataWithAPI("user/replyme/", "GET", params, true, function (res) {
      if (res.code == 0) {
        context.setData({
          list: page == 1 ? res.data.data : context.data.list.concat(res.data.data),
          current_page: res.data.current_page,
          total: res.data.total
        })
      }
    }, null, function () {
      context.hideWaitDialog();
      context.setData({
        isRefresh: false,
        showLoadMoreLoading: false
      });
    });
  },

  onPullDownRefresh: function () {
    this.showWaitDialog();
    this.setData({
      isRefresh: true
    });
    var context = this;
    wx.showNavigationBarLoading();
    this.loadData(1);
  },
  onReachBottom: function () {
    if (this.data.showLoadMoreLoading || this.data.isRefresh) {
      return;
    }
    if (this.data.current_page >= this.data.total) {
      return;
    }
    this.setData({
      showLoadMoreLoading: true
    });
    this.loadMore();
  },

  loadMore: function () {
    this.loadData(this.data.current_page + 1);
  },
  showWaitDialog: function () {
    util.showLoadingDialog("加载中...", 10000);
  },

  hideWaitDialog: function () {
    wx.hideNavigationBarLoading();
    wx.stopPullDownRefresh();
    wx.hideToast();
  },
  itemClick: function (res) {
    if (Date.parse(new Date()) - lastClickTime < 500 && Date.parse(new Date()) - lastClickTime >= 0) {
      return;
    }

    var item = res.currentTarget.dataset.item;
    wx.navigateTo({
      url: '../detail/detail?url={0}'.format(item.link),
      complete: function () {
        lastClickTime = Date.parse(new Date());
      }
    });
  }
})