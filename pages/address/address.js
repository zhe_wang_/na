let makeurl = require("../../utils/urls.js");
let dataManage = require("../../datamanage/apidatamanage.js");
let util = require('../../utils/util.js');
let evenBus = require('../../utils/event-bus');
let constant = require('../../utils/constant');
Page({
    data: {
        showInputAddress: true,
        addressList:[

        ]
    },
    onLoad: function (options) {
        // Do some initialize when page load.
        this.loadData();
    },
    onReady: function () {
        // Do something when page ready.
    },
    onShow: function () {
        // Do something when page show.
    },
    onHide: function () {
        // Do something when page hide.
    },
    onUnload: function () {
        // Do something when page close.
    },
    onPullDownRefresh: function () {
        // Do something when pull down.
    },
    onReachBottom: function () {
        // Do something when page reach bottom.
    },
    //onShareAppMessage: function () {
    // return custom share data when user share.
    //},
    onPageScroll: function () {
        // Do something when page scroll
    },

    loadData: function () {
      dataManage.requestDataWithAPI('user/my-address', 'GET', {}, true, res=>{
          this.setData({
              addressList: res.data
          })
      }, err=>{})
    },

    toggleInputAddress: function (e) {
      this.setData({
          showInputAddress: !this.data.showInputAddress
      })
    },

    onAddAddress: function (e) {
        let name = e.detail.value['input-name'];
        let phone = e.detail.value['input-phone'];
        let address = e.detail.value['input-address'];
        let country = e.detail.value['input-country'];

        if(util.isEmpty(name)) {
            util.showCommonModalDialog('请填写姓名');
            return;
        }
        if(util.isEmpty(phone)) {
            util.showCommonModalDialog('请填写手机号');
            return;
        }
        if(util.isEmpty(address)) {
            util.showCommonModalDialog('请填写详细地址');
            return;
        }
        if(util.isEmpty(country)) {
            util.showCommonModalDialog('请填写城市/省/国家');
            return;
        }
        let params = {
            address: address,
            country: country,
            user_name: name,
            phone: phone
        };
        util.showLoadingDialog('提交中');
        dataManage.requestDataWithAPI('user/build-address', 'POST', params, true, res=>{
            this.loadData();
        }, err=>{
            util.showCommonModalDialog(err.msg || '提交失败');
        }, ()=>{
            util.hideLoadingDialog();
        });

    },
    onAddressDeleteClicked: function (e) {
        util.showModalDialog('确定删除当前地址？', res=>{
            let item = e.currentTarget.dataset.item;
            let index = e.currentTarget.dataset.index;
            dataManage.requestDataWithAPI('user/del-address', 'POST', {id: item.id}, true, res=>{
                this.data.addressList.splice(index, 1);
                this.setData({
                    addressList: this.data.addressList
                })
            })
        });

    },
    onAddressSelectClicked: function (e) {
        let item = e.currentTarget.dataset.item;
        let index = e.currentTarget.dataset.index;
        evenBus.emit(constant.TAG_ADDRESS_SELECTED, item);
        setTimeout(()=>{
            let currentPages = getCurrentPages();
            if(currentPages && currentPages.length > 0 && currentPages[currentPages.length - 1] == this) { //防止倒计时没到用户自己返回导致接着返回会再往上返回一次
                wx.navigateBack();
            }
        }, 500);
    }


})