// pages/showimg.js
Page({

  /**
   * Page initial data
   */
  data: {
    url: "",
    count: 0
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {

    this.setData({
      url: options.url,
    });

  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {
    wx.previewImage({
      current: this.data.url, // 当前显示图片的http链接
      urls: [this.data.url] // 需要预览的图片http链接列表
    })
  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    console.log(this.data.count)
    this.setData({
      count: this.data.count + 1
    })
 
    if (this.data.count == 2){
      wx.navigateBack({
        delta: 1
      })
    }
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  }
})