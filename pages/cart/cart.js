let makeurl = require("../../utils/urls.js");
let dataManage = require("../../datamanage/apidatamanage.js");
let util = require('../../utils/util.js');
let eventBus = require('../../utils/event-bus');
let constant = require('../../utils/constant');

Page({
    data: {
        menu:[
            {
                id: 4,
                name: '已完成',
                count: 1
            },
            {
                id: 6,
                name: '等待付款',
                count: 0,
                icon: '../../images/icon-cart.png'
            },
            {
                id: 7,
                name: '积分',
                count: 0
            },
        ],
        doneOrders:{
            currentPage:0,
            hasMore: false,
            list:[]
        },
        carts:{
            currentPage:0,
            hasMore: false,
            list:[]
        },
        credits:{
            step: 0,
            account: '',
            credit: '',
            creditLog:[],
            loadedData: false,
            transferUser:{},
            count: '',
        },
        bindInput: '',
        currentSelectedIndex: 0,
    },
    onLoad: function (options) {
        let status = options.status || 6;
        for (let i = 0; i < this.data.menu.length; i++) {
            if (this.data.menu[i].id == status) {
                this.setData({
                    currentSelectedIndex: i
                });
                break;
            }
        }
        this.loadData(1);
        this.loadCartNum();
        evenBus.on(constant.TAG_CREDIT_DETACHED, this, data=>{
            let credits = data.data;
            this.data.credits.step = credits.step;
            this.data.credits.account = credits.account;
            this.data.credits.transferUser = credits.transferUser;
            this.data.credits.count = credits.count;
            this.data.credits.transferring = credits.transferring;

            this.setData({
                credits: credits
            });
        });
        eventBus.on(constant.TAG_ORDER_REFUND, this, (data) => {
            this.onRefund(data.order_id);
        });
    },
    onReady: function () {
        // Do something when page ready.
    },
    onShow: function () {
        // Do something when page show.
    },
    onHide: function () {
        // Do something when page hide.
    },
    onUnload: function () {
        // Do something when page close.
        eventBus.remove(constant.TAG_ORDER_REFUND, this);
    },
    onPullDownRefresh: function () {
        // Do something when pull down.
    },
    onReachBottom: function () {
        // Do something when page reach bottom.
    },
    //onShareAppMessage: function () {
    // return custom share data when user share.
    //},
    onPageScroll: function () {
        // Do something when page scroll
    },

    onMenuItemClicked: function (e) {
        let index = e.currentTarget.dataset.index;
        this.onMenuItemChanged(index);
    },

    onSwiperChanged: function(e) {
        let index = e.detail.current;
        if (index === this.data.currentSelectedIndex) {
            return;
        }
        this.onMenuItemChanged(index);
    },

    onMenuItemChanged: function (index) {
        let type = this.data.menu[index].id;
        let data = this.data.doneOrders;
        if (type === 6) {
            data = this.data.carts;
        } else if (type === 7) {
            data = this.data.credits;

          this.setData({
            currentSelectedIndex: index
          });
          this.loadData(1);
          return;
          
        }
        this.setData({
            currentSelectedIndex: index
        });
        if (data.currentPage <= 0) {
            this.loadData(1);
        }
    },

    loadData(page) {
        let type = this.data.menu[this.data.currentSelectedIndex].id;
        if (type == 4) {
            this.loadDoneOrderList(page);
        } else if (type == 6) {
            this.loadCarts(page);
        } else if (type == 7) {
            this.loadCredits();
        }

    },

    loadDoneOrderList: function (page) {
        let status = this.data.menu[this.data.currentSelectedIndex].id;
        let param = {
            page: page,
            status: status
        };

        dataManage.requestDataWithAPI('orderhelp/orderlist', 'GET', param, true, res=>{
            let {current_page, has_next_page, data} = res.data;
            let orders = this.data.doneOrders;
            orders.currentPage = current_page;
            orders.hasMore = has_next_page;
            orders.list = current_page == 1 ? data : [...orders.list, ...data];
            this.setData({
                doneOrders: orders
            });
        });
    },

    loadCarts: function (page) {
        dataManage.requestDataWithAPI('orderhelp/mycarts', 'GET', {page: page}, true, res=>{
            let {current_page, has_next_page, data} = res.data;
            let cart = this.data.carts;
            cart.currentPage = current_page;
            cart.hasMore = has_next_page;
            cart.list = current_page == 1 ? data : [...cart.list, ...data];
            this.setData({
                carts: cart
            });
        });
    },

    onScrollTop: function(e) {
        this.loadData(1);
    },
    onScrollBottom: function(e) {
        let type = this.data.menu[this.data.currentSelectedIndex].id;
        let data = this.data.doneOrders;
        if (type === 6) {
            data = this.data.carts;
        } else if (type === 7) {
            return;
        }
        if (data.list.length > 0 && data.hasMore) {
            this.loadData(data.currentPage + 1);
        }

    },
    onCartDeleted: function (e) {
        let cartId = e.detail.cartId;
        let carts = this.data.carts;

        for (let i = 0; i < carts.list.length; i++) {
            if(carts.list[i].id == cartId) {
                carts.list.splice(i, 1);
                break;
            }
        }

        this.setData({
            carts: carts
        });

    },
    onLikeClicked: function (e) {
      let newsId = e.detail.newsId;
      let cartId = e.detail.cartId;
      let count = e.detail.count;
      let carts = this.data.carts;

        for (let i = 0; i < carts.list.length; i++) {
            if(carts.list[i].news_id == newsId) {
                carts.list[i].ding_amount = count;
                // break;
            }
        }
        this.setData({
            carts: this.data.carts
        })
    },
    onRefund: function (orderId) {
        let doneOrders = this.data.doneOrders;
        for (let i = 0; i < doneOrders.list.length ; i++) {
            if (doneOrders.list[i].id == orderId) {
                doneOrders.list.splice(i, 1);
                break;
            }
        }
        this.setData({
            doneOrders: doneOrders
        })
    },

    onAddCollection: function (newsId) {

    },
    loadCartNum: function () {
        dataManage.requestDataWithAPI('orderhelp/mycarts-count', 'GET', {}, true, res=>{
            this.data.menu[1].count = res.data.total;
            this.setData({
                menu: this.data.menu
            })
        }, err=>{});
    },
    loadCredits: function () {
        dataManage.requestDataWithAPI('jifen/my-jifen', 'GET', {}, true, res => {
            let {jifen, jifen_log} = res.data;
            this.data.credits.credit = jifen;
            this.data.credits.creditLog = jifen_log;
            this.data.credits.loadedData = true;
            this.setData({
                credits: this.data.credits
            });
            wx.pageScrollTo({
                scrollTop: 0,
                duration: 1
            });
        }, err=>{});
    },
  onMoreClicked: function (e) {
    wx.navigateTo({
      url: '/pages/category/category'
    })
  }
})