
// pages/transpoint/transpoint.js
// pages/purchase/purchase.js
var url = require("../../utils/urls.js");
var dataManage = require("../../datamanage/apidatamanage.js");
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:"",
    point:"",
    showInfo: false,
    avatar:"",
    nickname:""
  },
  bindInputId: function (e) {
    this.setData({
      id: e.detail.value
    })
  },
  bindInputPoint: function (e) {
    this.setData({
      point: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  transpoint:function(){
    if (!this.data.showInfo){
      wx.showToast({
        title: '请先输入对方Id点击确认',
        icon: 'none',
        duration: 2000
      })
    }
    else if (this.data.point == ""){
      wx.showToast({
        title: '请输入要转的积分数量',
        icon: 'none',
        duration: 2000
      })
    }else {

      var that = this
      wx.showModal({
        title: '提示',
        content: '请确认将' + this.data.point+'积分转给'+this.data.nickname+"？",
        success: function (res) {
          if (res.confirm) {
            var params = {
              id: that.data.id,
              jifen: that.data.point,
            };
            dataManage.requestDataWithAPI("user/attornjifen", "POST", params, true, function (res) {
              if (res.code == 0) {
                wx.showToast({
                  title: '操作成功',
                  icon: 'none',
                  duration: 2000
                })
                setTimeout(function () {
                  wx.navigateBack()
                }, 2000) //延迟时间 这里是1秒  
              } 
            }, function (res) {
              wx.showToast({
                title: res.message,
                icon: 'none',
                duration: 2000
              })

            }, function () {

            });
          } else if (res.cancel) {
            
          }
        }
      })

    }
  },
  search:function(){
    if(this.data.id == ""){
      wx.showToast({
        title: '请输入对方ID',
        icon: 'none',
        duration: 2000
      })
    }else{
      this.getUserInfo()
    }
  },
  getUserInfo:function(){
    var that = this
    var params = {
      id: this.data.id,
    };
    dataManage.requestDataWithAPI("user/getuser", "GET", params, true, function (res) {
      if (res.code == 0) {
        that.setData({
          avatar: res.data.avatar,
          nickname: res.data.nickname,
          showInfo: true
        });
      } else {
        that.setData({
          showInfo: false
        });

        access_token = null;
      }
    }, function (res) {

    }, function () {

    });
  }
})