
var url = require("../../utils/urls.js");
var dataManage = require("../../datamanage/apidatamanage.js");
var util = require('../../utils/util.js');
//获取应用实例
var app = getApp()
Page({
  data: {
    userInfo: {},
    isRefresh: true,
    showLoadMoreLoading: false,
    currentChannelId: "normal_action",
    currentChannelName: "最新",
    currentTypeId: "0",
    currentTypeName: "最热",
    currentStatusId: "9",
    currentStatusName: "分类",
    max_page: 0,
    current_page: 0
  },
  //事件处理函数
  onItemClicked: function (event) {
    console.log(event);
    var eventId = event.currentTarget.dataset.item.id;
    var eventCategory = event.currentTarget.dataset.item.category;
    wx.navigateTo({
      url: '../detail/detail?category={0}&id={1}'.format(eventCategory, eventId)
    });
  },
  onLoad: function () {
    console.log('onLoad');
    this.showWaitDialog();
    this.onLoadNavigation();
    this.loadData(this.data.currentChannelId, this.data.currentTypeId, this.data.currentStatusId, 1);
  },

  loadData: function (channelId, typeId, statusId, page) {
    var context = this;
    context.showWaitDialog();
    var params = {
      category: channelId,
      tagcode: typeId,
      status: statusId,
      page: page
    }
    dataManage.requestDataWithAPI(url.API_HOMEPAGE, "GET", params, false, function (res) {
      if (res.code == 0) {
        var data = res.data.data;
        context.setData({
          current_page: res.data.page,
          max_page: res.data.max_page
        });
        for (var item in data) {
          data[item]["start_str"] = util.formatTime(new Date(data[item]["start"] * 1000));
        }
      }
    }, null, function () {
      context.hideWaitDialog();
      context.setData({
        isRefresh: false,
        showLoadMoreLoading: false
      });
    });
  },

  showWaitDialog: function () {
    util.showLoadingDialog("加载中...", 10000);
    // wx.showToast({
    //   title: "加载中",
    //   icon: "loading",
    //   duration: 10000,
    //   mask: true
    // })
  },

  hideWaitDialog: function () {
    wx.hideNavigationBarLoading();
    wx.stopPullDownRefresh();
    wx.hideToast();
  },

  onPullDownRefresh: function () {
    this.showWaitDialog();
    this.setData({
      isRefresh: true
    });
    var context = this;
    wx.showNavigationBarLoading();
    this.loadData(this.data.currentChannelId, this.data.currentTypeId, this.data.currentStatusId, 1);
  },
  onReachBottom: function () {
    if (this.data.showLoadMoreLoading || this.data.isRefresh) {
      return;
    }
    if(this.data.current_page >= this.data.max_page) {
      return;
    }
    this.setData({
      showLoadMoreLoading: true
    });
    this.loadMore();
  },

  loadMore: function() {
    this.loadData(this.data.currentChannelId, this.data.currentTypeId, this.data.currentStatusId, this.data.current_page + 1);
  },
  onMenuClicked: function (event) {
    var type = event.target.dataset.type;
    this.showMenu(type);
  },
  onMenuClickedHot: function (event) {
    this.onStatusMenuItemClicked(event);
    
  },
  onMenuClickedLatest: function (event) {
    this.onStatusMenuItemClicked(event);
  },
  
  onChannelMenuItemClicked: function (event) {
    this.onFullBgClicked(event);
    var item = event.target.dataset.item;
    if (item.channelId == this.data.currentChannelId) {
      return;
    }
    this.setData({
      currentChannelId: item.channelId,
      currentChannelName: item.channelName,
      currentTypeId: "0",
      currentTypeName: "全部",
      isRefresh: true
    });
    this.loadData(this.data.currentChannelId, this.data.currentTypeId, this.data.currentStatusId, 1);
  },

  onTypeMenuItemClicked: function (event) {
    this.onFullBgClicked(event);
    var item = event.target.dataset.item;
    if (item.typeId == this.data.currentTypeId) {
      return;
    }
    this.setData({
      currentTypeId: item.typeId,
      currentTypeName: item.typeName,
      isRefresh: true
    });
    this.loadData(this.data.currentChannelId, this.data.currentTypeId, this.data.currentStatusId, 1);
  },

  onStatusMenuItemClicked: function (event) {
    this.onFullBgClicked(event);
    var item = event.target.dataset.item;
    if (item.statusId == this.data.currentStatusId) {
      return;
    }
    this.setData({
      currentStatusId: item.statusId,
      currentStatusName: item.statusName,
      isRefresh: true
    });
    this.loadData(this.data.currentChannelId, this.data.currentTypeId, this.data.currentStatusId, 1);
  },

  onTapMoved: function(res) {
    console.log(res);
  },
  onShareAppMessage: function () {
    return {
      title: "活动中心",
      desc: "",
      path: "/pages/index/index"
    }
  }

})
