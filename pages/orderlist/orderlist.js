let dataManage = require("../../datamanage/apidatamanage.js");
let util = require('../../utils/util.js');
let eventBus = require('../../utils/event-bus');
let constant = require('../../utils/constant');
Page({
    data: {
        menu:[
            {
                id: 5,
                name: '完成',
                count: 0
            },
            {
                id: 7,
                name: '等待收货',
                count: 0
            },
            {
                id: 6,
                name: '退货',
                count: 0
            },
        ],
        doneOrders:{
            currentPage:0,
            hasMore: false,
            list:[]
        },
        pendingOrders:{
            currentPage:0,
            hasMore: false,
            list:[]
        },
        refundOrders:{
            currentPage:0,
            hasMore: false,
            list:[]
        },
        currentSelectedIndex: 0,
    },
    onLoad: function (options) {
        let status = options.status || 5;
        for (let i = 0; i < this.data.menu.length; i++) {
            if (this.data.menu[i].id == status) {
                this.setData({
                    currentSelectedIndex: i
                });
                break;
            }
        }
        this.loadData(1);
        this.loadPendingCount();
        eventBus.on(constant.TAG_ORDER_REFUND, this, (data) => {
            this.onRefund(data.order_id);
        });
    },
    onReady: function () {
        // Do something when page ready.
    },
    onShow: function () {
        // Do something when page show.
    },
    onHide: function () {
        // Do something when page hide.
    },
    onUnload: function () {
        eventBus.remove(constant.TAG_ORDER_REFUND, this);
    },
    onPullDownRefresh: function () {
        // Do something when pull down.
    },
    onReachBottom: function () {
        // Do something when page reach bottom.
    },
    //onShareAppMessage: function () {
    // return custom share data when user share.
    //},
    onPageScroll: function () {
        // Do something when page scroll
    },
    onMenuItemChanged: function (index) {
        let orders = this.data.doneOrders;
        if (index == 1) {
            orders = this.data.pendingOrders;
        } else if (index == 2) {
            orders = this.data.refundOrders;
        }
        this.setData({
            currentSelectedIndex: index
        });
        if (orders.currentPage <= 0) {
            this.loadData(1);
        }
    },
    loadData(page) {
        let status = this.data.menu[this.data.currentSelectedIndex].id;
        let param = {
            page: page,
            status: status
        };

        dataManage.requestDataWithAPI('orderhelp/orderlist', 'GET', param, true, res=>{
            let {current_page, has_next_page, data} = res.data;
            let orders = this.data.doneOrders;
            if (status == 1) {
                orders = this.data.privateMsg;
            } else if (status == 7) {
                orders = this.data.pendingOrders
            } else if (status == 6) {
                orders = this.data.refundOrders;
            }
            orders.currentPage = current_page;
            orders.hasMore = has_next_page;
            orders.list = current_page == 1 ? data : [...orders.list, ...data];
            if (status == 5) {
                this.setData({
                    doneOrders: orders
                });
            } else if (status == 6) {
                this.setData({
                    refundOrders: orders
                });
            } else if (status == 7) {
                this.setData({
                    pendingOrders: orders
                });
            }

        });
    },

    onMenuItemClicked: function (e) {
        let index = e.currentTarget.dataset.index;
        this.onMenuItemChanged(index);
    },

    onSwiperChanged: function(e) {
        let index = e.detail.current;
        if (index === this.data.currentSelectedIndex) {
            return;
        }
        this.onMenuItemChanged(index);
    },

    onScrollTop: function(e) {
        this.loadData(1);
    },
    onScrollBottom: function(e) {
        let status = this.data.menu[this.data.currentSelectedIndex].id;
        let orders = this.data.doneOrders;
        if (status == 7) {
            orders = this.data.pendingOrders;
        } else if (status == 6) {
            orders = this.data.refundOrders;
        }
        if (orders.list.length > 0 && orders.hasMore) {
            this.loadData(orders.currentPage + 1);
        }
    },
    onRefund: function (orderId) {
        let doneOrders = this.data.doneOrders;
        let pendingOrders = this.data.pendingOrders;
        for (let i = 0; i < doneOrders.list.length ; i++) {
            if (doneOrders.list[i].id == orderId) {
                doneOrders.list.splice(i, 1);
                break;
            }
        }
        for (let i = 0; i < pendingOrders.list.length ; i++) {
            if (pendingOrders.list[i].id == orderId) {
                pendingOrders.list.splice(i, 1);
                break;
            }
        }
        this.setData({
            doneOrders: doneOrders,
            pendingOrders: pendingOrders
        })
    },
    loadPendingCount: function () {
        dataManage.requestDataWithAPI('orderhelp/receipt-stats', 'GET', {}, true, res=>{
            this.data.menu[1].count = res.data.total;
            this.setData({
                menu: this.data.menu
            });
        }, err=>{});
    },
  onMoreClicked: function (e) {
    wx.navigateTo({
      url: '/pages/category/category'
    })
  },
})