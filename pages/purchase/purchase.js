// pages/purchase/purchase.js
var url = require("../../utils/urls.js");
var dataManage = require("../../datamanage/apidatamanage.js");
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    p_id:0,
    product:{},
    pay_type:"",
    inputName: "",
    inputAddress: "",
    inputNumber: "",
    inputPhone: ""
  },
  radioChange: function (e) {
    this.setData({
      pay_type: e.detail.value
    })
  },
  bindKeyInputNumber: function (e) {
    this.setData({
      inputNumber: e.detail.value
    })
  }, 
  bindKeyInputName: function (e) {
    this.setData({
      inputName: e.detail.value
    })
  },
  bindKeyInputAddress: function (e) {
    this.setData({
      inputAddress: e.detail.value
    })
  },
  bindKeyInputPhone: function (e) {
    this.setData({
      inputPhone: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      p_id: options.p_id,
      inputName: wx.getStorageSync('inputName'),
      inputAddress: wx.getStorageSync('inputAddress'),
      inputPhone: wx.getStorageSync('inputPhone'),
    });

    this.loadData();

  },

  loadData: function (page) {
    var context = this;
    dataManage.requestDataWithAPI("order/product/" + this.data.p_id, "GET",null, false, function (res) {
      if (res.code == 0) {
        context.setData({
          product: res.data
        })
      }
    }, null, null);
  },
  formSubmit: function (e) {
    wx.showLoading({
      title: '订单提交中',
    })
    this.postComment()
  },
  postComment: function () {
    var that = this;
    var params = {
      id: this.data.p_id,
      inputName: this.data.inputName,
      inputAddress: this.data.inputAddress,
      inputPhone: this.data.inputPhone,
      pay_type: this.data.pay_type,
      num: this.data.inputNumber
    };
    dataManage.requestDataWithAPI("/order/checkout/", "POST", params, true,
      function (res) {
        wx.showModal({
          title: '支付确认',
          content: res.data.award_jifen + '\r\n' + res.data.cost_jifen + '\r\n购买数量：' + res.data.num + '\r\n支付方式：' + res.data.pay_type_desc + '\r\n价格:' + res.data.total_price + '\r\n收货人姓名:' + that.data.inputName + '\r\n地址:' + that.data.inputAddress + '\r\n电话:' + that.data.inputNumber,
          success: function (res) {
            if (res.confirm) {
              dataManage.requestDataWithAPI("/order/pay/", "POST", params, true,
                function (res) {
                  wx.setStorageSync('inputName', that.data.inputName)
                  wx.setStorageSync('inputAddress', that.data.inputAddress)
                  wx.setStorageSync('inputPhone', that.data.inputPhone)
                  wx.showModal({
                    title: '支付成功',
                    showCancel:false,
                    content: '订单编号:'+res.data.order_no, success: function (res) {
                      wx.navigateBack();
                    }
                  })
                }, function (res2) {

                  wx.showModal({
                    title: '系统提示',
                    content: res2.message,
                    showCancel:fasel,
                    success: function (res) {
                        wx.hideLoading();
                      
                    }
                  })
                }, function () {
                  wx.hideLoading();
                });
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }, function (res) {

        wx.showToast({
          title: res.message,
          icon: 'none',
          duration: 1000
        })
      }, function () {
        wx.hideLoading();
      });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
  
})