var url = require("../../utils/urls.js");
var dataManage = require("../../datamanage/apidatamanage.js");
var util = require('../../utils/util.js');
var lastClickTime = 0;
var app = getApp();
Page({
  data: {
    list:[],
    isRefresh: true,
    showLoadMoreLoading: false,
    total: 0,
    current_page: 0,
  },
  onLoad: function (options) {
    this.loadData(1);
  },
  onReady: function () {
    
  },
  onShow: function () {
    // 页面显示
    
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },

  onMenuClickedLatest: function (){
    this.setData({
      isHot:false
    })

    this.onPullDownRefresh();
  },
  onMenuClickedHot: function () {
    this.setData({
      isHot: true
    })
    this.onPullDownRefresh();
  },
  onMenuClicked: function () {
      wx.navigateTo({
        url: '../menu/menu',
        
      })
  },
  ding: function(res){
    var item = res.currentTarget.dataset.item;
    this.setData({
      ['list[' + res.currentTarget.dataset.index + '].ding_amount']: res.currentTarget.dataset.item.ding_amount + 1
    })
    this.vote(1, item.id)
  },
  cai: function (res) {
    var item = res.currentTarget.dataset.item;
    this.setData({
      ['list[' + res.currentTarget.dataset.index + '].cai_amount']: res.currentTarget.dataset.item.cai_amount + 1
    })
    this.vote(2, item.id)
  },
  vote: function (vote_type,news_id) {
    var params = { type: vote_type };
    dataManage.requestDataWithAPI("news/event/"+news_id, "POST", params, true, function (res) {
      if (res.code == 0) {
        wx.showToast({
          title: '操作成功',
        })
      }
    }, function(res){
      wx.showToast({
        title: res.message,
        icon: "none"
      })
    }, function () {
      
    });
  },
  loadData: function(page) {
    this.showWaitDialog();
    var context = this;
    var params = { page: page };
    dataManage.requestDataWithAPI("/news/like/list", "GET", params, true, function(res) {
      if (res.code == 0) {
        context.setData({
          list: page == 1 ? res.data.data : context.data.list.concat(res.data.data),
          current_page: res.data.current_page,
          total: res.data.total
        })
      }
    }, null, function () {
      context.hideWaitDialog();
      context.setData({
        isRefresh: false,
        showLoadMoreLoading: false
      });
    });
  },
  
  onPullDownRefresh: function () {
    this.showWaitDialog();
    this.setData({
      isRefresh: true
    });
    var context = this;
    wx.showNavigationBarLoading();
    this.loadData(1);
  },
  onReachBottom: function () {
    if (this.data.showLoadMoreLoading || this.data.isRefresh) {
      return;
    }
    if(this.data.current_page >= this.data.total) {
      return;
    }
    this.setData({
      showLoadMoreLoading: true
    });
    this.loadMore();
  },

  loadMore: function() {
    this.loadData(this.data.current_page + 1);
  },
  showWaitDialog: function () {
    util.showLoadingDialog("加载中...", 10000);
  },

  hideWaitDialog: function () {
    wx.hideNavigationBarLoading();
    wx.stopPullDownRefresh();
    wx.hideToast();
  },
  itemClick: function (res) {
    if (Date.parse(new Date()) - lastClickTime < 500 && Date.parse(new Date()) - lastClickTime >= 0) {
      return;
    }

    var item = res.currentTarget.dataset.item;
      wx.navigateTo({
        url: '../detail/detail?url={0}'.format(item.link),
        complete: function() {
          lastClickTime = Date.parse(new Date());
        }
      });
   

  },
  onShareAppMessage: function () {
    return {
      title: "新闻",
      desc: "新闻",
      path: "/pages/main/main"
    }
  }
})