// pages/commentlist/commentlist.js
let url = require("../../utils/urls.js");
let dataManage = require("../../datamanage/apidatamanage.js");
let util = require('../../utils/util.js');
let eventBus = require('../../utils/event-bus.js');
let constant = require('../../utils/constant');
let lastClickTime = 0;
let app = getApp();
import LruCach from '../../utils/LRUCache';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        current_page: 0,
        total: 0,
        category_id: '',
        news_id: 0,
        list: [],
        scrollMenu: [],
        scrollToView: 'menu_0',
        currentSelectedIndex: 0,
        data: {},
    },

    lruCache: new LruCach(3), //防止分类页面过多导致页面卡顿(对每个页面的 item 进行操作 例如收藏点赞啥的 需要同步更新到缓存)
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

        this.setData({
            news_id: options.news_id,
            share_id: options.share_id
        });

        this.setData({
          category_id: 'new:' + this.data.news_id,
          scrollMenu: [{
            id: 'new:' + this.data.news_id,
            name: '最新',
            count: 0,
          },
          {
            id: 'hot:' + this.data.news_id,
            name: '最热',
            count: 0,
          },
          {
            id: 'filter:' + this.data.news_id,
            name: '@我的',
            count: 0,
          },
          {
            id: 'post:' + this.data.news_id,
            name: '我发布的',
            count: 0,
          },
          ]
        })
        this.loadData(this.data.category_id);
        if(dataManage.isLogin()){
        this.loadAtMe();
        }
        eventBus.on(constant.TAG_COMMENT_DELETED, this, data => {
            this.deleteMyComment(data.comment_id);
        });
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        if (this.innerAudioContext) {
            this.innerAudioContext.destroy();
            this.innerAudioContext = null;
        }
        eventBus.remove(constant.TAG_COMMENT_DELETED, this);
    },


    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },

    getLRUCacheName: function (id) {
        for (let i = 0; i < this.data.scrollMenu.length; i++) {
            if (id == this.data.scrollMenu[i].id) {
                return this.data.scrollMenu[i].name;
            }
        }
    },

    loadAtMe: function () {
        dataManage.requestDataWithAPI('comment/atmy', 'GET', {news_id: this.data.news_id}, true, res=>{
            for(let i = 0; i < this.data.scrollMenu.length; i++) {
                if(this.data.scrollMenu[i].id === `filter:${this.data.news_id}`) {
                    this.data.scrollMenu[i].count = res.data.unread || this.data.scrollMenu[i].count;
                    break;
                }
            }
            this.setData({
                scrollMenu: this.data.scrollMenu
            })
        }, err=>{});
    },

    // resetAtMe: function () {
    //     dataManage.requestDataWithAPI('comment/reset-at-my', 'POST', {news_id: this.data.news_id}, true, res=>{
    //         this.setData({
    //             scrollMenu: 0
    //         })
    //     }, err=>{});
    // },

    loadData: function (id) {
        let lruCacheName = this.getLRUCacheName(id);
        if (!this.data.data[lruCacheName]) {
            this.loadDataFromAPI(id, 1);
        } else {
            this.lruCache.put(lruCacheName, this.data.data[lruCacheName]);
        }
    },
    loadDataFromAPI: function (id, page) {
        this.showWaitDialog();
        let [preId, postId] = `${id}`.split(':');
        var context = this;
        var hot = '0'
        var fresh = '0'
        var ismy = '0'
        var is_repay_me = '0'
      if (this.data.currentSelectedIndex == 0) {
          fresh = '1'
        }
      if (this.data.currentSelectedIndex == 1) {
          hot = '1'
        }
      if (this.data.currentSelectedIndex == 2) {
           is_repay_me = '1'
        }
      if (this.data.currentSelectedIndex == 3) {
            ismy = '1'
      }
        var params = {
            page: page,
            hot: hot,
            fresh: fresh,
            is_my: ismy,
            is_repay_me: is_repay_me,
            id: this.data.share_id
        };
      dataManage.requestDataWithAPI("comment/news/" + postId+ "/list", "GET", params, true, function (res) {
            if (res.code == 0) {
                let lruCacheName = context.getLRUCacheName(id);
                context.data.data[lruCacheName] = {
                    current_page: page,
                    total: res.data.total,
                    list: page === 1 ? res.data.data : [...context.data.data[lruCacheName].list, ...res.data.data]
                };
                let removed = context.lruCache.put(lruCacheName, context.data.data[lruCacheName]);

                if (removed) {
                    context.data.data[removed.key] = undefined;
                }
                context.setData({
                    data: context.data.data
                })

            }
        }, null, function () {
            context.hideWaitDialog();
            context.setData({
                isRefresh: false,
                share_id: -1,
                showLoadMoreLoading: false
            });
        });
    },

    onSwiperChanged: function (e) {
        let index = e.detail.current;
        if (index === this.data.currentSelectedIndex) {
            return;
        }

        this.changeScrollViewSelectedView(index);
    },
    onMenuItemClicked: function (e) {
        let index = e.currentTarget.dataset.index;
        if (index === this.data.currentSelectedIndex) {
            return;
        }

        // this.setData({
        //     scrollMenu: this.getFilterMenuDynamic(index)
        // });
        this.changeScrollViewSelectedView(index);
        // this.setData({
        //     currentSelectedIndex: index
        // });
        // this.loadData(this.data.id);
    },

    changeScrollViewSelectedView: function (curIndex) {

        let selectedIndex = curIndex;

        if (curIndex > 0) {
            selectedIndex = curIndex - 1;
        }
        this.setData({
            scrollToView: `menu_${selectedIndex}`,
            currentSelectedIndex: curIndex
        });

        this.loadData(this.data.scrollMenu[curIndex].id);
    },
    onPullDownRefresh: function () {
        this.showWaitDialog();
        this.setData({
            isRefresh: true
        });
        var context = this;
        wx.showNavigationBarLoading();
      this.loadData(this.data.scrollMenu[this.data.currentSelectedIndex].id);
    },
    onReachBottom: function () {
        if (this.data.showLoadMoreLoading || this.data.isRefresh) {
            return;
        }
        if (this.data.current_page >= this.data.total) {
            return;
        }
        this.setData({
            showLoadMoreLoading: true
        });
        this.loadMore();
    },


    loadMore: function (id) {
        let lruCacheName = this.getLRUCacheName(id);
        let node = this.data.data[lruCacheName];
      if (node.current_page && node.total && node.current_page < node.total) {
        this.loadDataFromAPI(id, node.current_page + 1);
        }
    },

    refresh: function (id) {
        this.loadDataFromAPI(id, 1);
    },

    onScrollTop: function (e) {
        this.refresh(this.data.scrollMenu[this.data.currentSelectedIndex].id);
    },
    onScrollBottom: function (e) {
        let id = this.data.scrollMenu[this.data.currentSelectedIndex].id;
        let name = this.data.scrollMenu[this.data.currentSelectedIndex].name;

        if (!this.data.data[name]) {
            return;
        }
        this.loadMore(id);
    },


    showWaitDialog: function () {
        util.showLoadingDialog("加载中...", 10000);
    },

    hideWaitDialog: function () {
        wx.hideNavigationBarLoading();
        wx.stopPullDownRefresh();
        wx.hideToast();
    },
    itemClick: function (res) {
        if (Date.parse(new Date()) - lastClickTime < 500 && Date.parse(new Date()) - lastClickTime >= 0) {
            return;
        }

        var item = res.currentTarget.dataset.item;
        wx.navigateTo({
            url: '../detail/detail?url={0}'.format(item.link),
            complete: function () {
                lastClickTime = Date.parse(new Date());
            }
        });
    },
    reply: function (res) {
        var res = res;
        var item = res.currentTarget.dataset.item;
        wx.navigateTo({
            url: '../comment/comment?news_id=' + this.data.news_id + "&comment_id=" + item.id + "&is_private=" + item.is_private,
            complete: function () {
                lastClickTime = Date.parse(new Date());
            }
        });
    },
    comment: function () {
        if(dataManage.isLogin()){
        wx.navigateTo({
            url: '../comment/comment?news_id=' + this.data.news_id,
            complete: function () {
                lastClickTime = Date.parse(new Date());
            }
        });
        }else{
          wx.navigateTo({
            url: '../usercenter/usercenter',
          })
        }
    },
    showPerson: function (res) {
        var res = res;
        var item = res.currentTarget.dataset.item;
        wx.navigateTo({
            url: '../commentlist/commentlist?news_id=' + this.data.news_id,
            complete: function () {
                lastClickTime = Date.parse(new Date());
            }
        });
    },
    ding: function (res) {
        var res = res;
        var item = res.currentTarget.dataset.item;
        var that = this;
        this.vote(1, item.id, function () {
            that.setData({
                ['list[' + res.currentTarget.dataset.index + '].ding_amount']: res.currentTarget.dataset.item.ding_amount + 1
            })
        })
    },
    cai: function (res) {

        var res = res;
        var item = res.currentTarget.dataset.item;
        var that = this;
        this.setData({
            ['list[' + res.currentTarget.dataset.index + '].cai_amount']: res.currentTarget.dataset.item.cai_amount + 1
        })
        this.vote(2, item.id, function () {
            that.setData({
                ['list[' + res.currentTarget.dataset.index + '].ding_amount']: res.currentTarget.dataset.item.ding_amount + 1
            })
        })
    },
    vote: function (vote_type, news_id, cb) {
        var params = {
            type: vote_type
        };
        dataManage.requestDataWithAPI("comment/event/" + news_id, "POST", params, true, function (res) {
            if (res.code == 0) {
                wx.showToast({
                    title: '操作成功',
                })
                cb();
            }
        }, function (res) {
            wx.showToast({
                title: res.message,
                icon: "none"
            })
        }, function () {

        });
    },
    onVoiceClicked: function (e) {
        let {start, id, parentId, audioPath} = e.detail;
        if (start) {
            this.playAudio(id, audioPath);
        } else {
            this.stopAudio(id, audioPath);
        }
    },
    playAudio: function (id, audioPath) {
        if (!audioPath) {
            return
        }
        if (this.innerAudioContext) {
            this.innerAudioContext.stop();
        }
        eventBus.emit(constant.TAG_STOP_VOICE, {currentId: id});
        this.innerAudioContext = wx.createInnerAudioContext();
        this.innerAudioContext.onPlay(() => {
            console.log('开始播放');
        });
        this.innerAudioContext.onError((res) => {
            console.log('播放出错');
            console.log(res);
            eventBus.emit(constant.TAG_STOP_VOICE, {currentId: 0});
        });
        this.innerAudioContext.onEnded((res) => {
            console.log('播放结束');
            console.log(res);
            eventBus.emit(constant.TAG_STOP_VOICE, {currentId: 0});
        });
        this.innerAudioContext.src = audioPath; // 这里可以是录音的临时路径
        this.innerAudioContext.play();
    },

    stopAudio: function (id, audioPath) {
        if (this.innerAudioContext) {
            // this.innerAudioContext.stop();
            this.innerAudioContext.destroy();
            eventBus.emit(constant.TAG_STOP_VOICE, {currentId: id});
        }
    },
    deleteMyComment: function (commentId) {
        let lruCacheName = this.getLRUCacheName(`post:${this.data.news_id}`);
        if(this.data.data[lruCacheName] && this.data.data[lruCacheName].list) {
            for (let i = 0; i < this.data.data[lruCacheName].list.length; i++) {
                let item = this.data.data[lruCacheName].list[i];
                if(item.id == commentId) {
                    this.data.data[lruCacheName].list.splice(i, 1);
                }
            }
        }
        this.lruCache.put(lruCacheName, this.data.data[lruCacheName]);
        this.setData({
            data: this.data.data
        })
    },


  onMoreClicked: function (e) {
    wx.navigateTo({
      url: '/pages/category/category'
    })
  },

  onShareAppMessage: (res) => {
    console.log('/pages/commentlist/commentlist?news_id={0}&share_id={1}'.format(res.target.dataset.nid, res.target.dataset.cid))
    return {
      title: res.target.dataset.title,
      path: '/pages/commentlist/commentlist?news_id={0}&share_id={1}'.format(res.target.dataset.nid, res.target.dataset.cid),
      success: (res) => {
        console.log("转发成功", res);
      },
      fail: (res) => {
        console.log("转发失败", res);
      }
    }
  }
})
