let makeurl = require("../../utils/urls.js");
let dataManage = require("../../datamanage/apidatamanage.js");
let util = require('../../utils/util.js');
let evenBus = require('../../utils/event-bus');
let constant = require('../../utils/constant');
Page({
    data: {
        id: 0,
        showInputRemark: true,
        tags:[

        ],
        selectedTags:{

        },
        remark:''
    },
    onLoad: function (options) {
        this.data.id = options.id;
        this.loadData();
    },
    onReady: function () {
        // Do something when page ready.
    },
    onShow: function () {
        // Do something when page show.
    },
    onHide: function () {
        // Do something when page hide.
    },
    onUnload: function () {
        // Do something when page close.
    },
    onPullDownRefresh: function () {
        // Do something when pull down.
    },
    onReachBottom: function () {
        // Do something when page reach bottom.
    },
    //onShareAppMessage: function () {
    // return custom share data when user share.
    //},
    onPageScroll: function () {
        // Do something when page scroll
    },

    loadData: function () {
        dataManage.requestDataWithAPI("order/product/" + this.data.id, "GET",null, false, (res)=> {
            this.setData({
                tags: res.data.tags,
            })
        }, null, null);
    },

    onSubmit: function (e) {
        let remark = this.data.remark;
        let tags = [];
        for(let i = 0; i < this.data.tags.length; i++) {
            if (this.data.selectedTags[this.data.tags[i]] == 1) {
                tags.push(this.data.tags[i]);
            }
        }
        evenBus.emit(constant.TAG_REMARK_SELECTED, {remark: remark, tags: tags});
        setTimeout(()=>{
            let currentPages = getCurrentPages();
            if(currentPages && currentPages.length > 0 && currentPages[currentPages.length - 1] == this) { //防止倒计时没到用户自己返回导致接着返回会再往上返回一次
                wx.navigateBack();
            }
        }, 500);
    },
    toggleInputRemark: function (e) {
        this.setData({
            showInputRemark: !this.data.showInputRemark
        });
    },
    onTagItemClicked: function (e) {
        let item = e.currentTarget.dataset.item;
        this.data.selectedTags[item] = this.data.selectedTags[item] ? 0 : 1;
        this.setData({
            selectedTags: this.data.selectedTags
        })
    },
    onInput: function (e) {
        this.data.remark = e.detail.value;
    }
})