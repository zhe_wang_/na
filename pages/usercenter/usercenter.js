var app = getApp();
var util = require("../../utils/util.js");
var urls = require("../../utils/urls.js");
var datamanage = require("../../datamanage/apidatamanage.js");
//var currentMothIndex = 0;
Page({
  data: {
    userInfo: {},
    code:"",
    userDisplayInfo: "",
    currentMothIndex: 0,
    isRefresh: true,
    needLogin: false,
    showLoadMoreLoading: false,
      myMsgCount: 0,
      cartCount: 0,
      waitReceiveCount: 0,
      waitRefundCount: 0

  },
  onLoad: function (options) {
    var that = this;
    //调用登录接口
    wx.login({
      success: function (loginRes) {
        if (loginRes.code) {
          that.setData({
            code: loginRes.code
          })
        }
      }
    });
  },
  onReady: function () {
  },

  getUserInfo: function () {
    // 页面渲染完成
    var that = this;
    var access_token = wx.getStorageSync('access_token');
    if (access_token == null || access_token == "") {
      that.setData({
        needLogin: true
      });
    } else {

      datamanage.requestDataWithAPI("user/myhome", "GET", null, true, function (res) {
        if (res.code == 0) {
          that.setData({
            userInfo: res.data.user_info,
              cartCount: res.data.at_me,
              waitReceiveCount: res.data.wait_recive,
              waitRefundCount: res.data.wait_refund,
            needLogin: false
          });
        } else {
          that.setData({
            needLogin: true
          });

          access_token = null;
        }
      }, function (res) {

      }, function () {

      });
    }
  },
  onShow: function () {

    this.getUserInfo()
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },

  showWaitDialog: function () {
    util.showLoadingDialog("加载中...", 10000);
  },

  hideWaitDialog: function () {
    wx.hideNavigationBarLoading();
    wx.stopPullDownRefresh();
    wx.hideToast();
    this.setData({
      isRefresh: false,
      showLoadMoreLoading: false
    });
  },

  onGotUserInfo: function (res) {
    var that = this
    var access_token;
    var encryptedData = res.detail.encryptedData
    var iv = res.detail.iv
    wx.setStorageSync('avatar', res.detail.userInfo.avatarUrl);
    wx.setStorageSync('uname', res.detail.userInfo.nickName);
    var signature = this.data.code
    datamanage.requestDataWithAPI(urls.API_AUTH, "GET", { js_code: signature, encryptedData: encryptedData, iv: iv }, false, function (res) {
      if (res.code == 0) {
          access_token = res.data.access_token;
          wx.setStorageSync('access_token', access_token);
          that.getUserInfo()
          wx.navigateBack()
      } else {
        access_token = null;
      }
    }, function (res) {

    }, function () {

    });
  },

  showMailBox: function (){
    wx.navigateTo({
      url: '../mailbox/mailbox',
    })
  },


  showTransPoint: function () {
    wx.navigateTo({
      url: '../transpoint/transpoint',
    })
  },

  showOrders: function () {
    wx.navigateTo({
      url: '../cart/cart',
    })
  },
  showVirtualOrders: function (){
    wx.navigateTo({
      url: '../virtualorderlist/virtualorderlist',
    })
  },
    showAssets: function () {
        wx.navigateTo({
          url: '/pages/myassets/myassets'
        })
    },
    showFavorite: function () {
        wx.navigateTo({
            url: '/pages/myfavorite/myfavorite'
        })
    },
    showMsg: function (e) {
        wx.navigateTo({
          url: '/pages/mymessages/mymessages'
        })
    },
    showOrders: function (e) {
        let status = e.currentTarget.dataset.status;
        wx.navigateTo({
          url: `/pages/orderlist/orderlist?status=${status}`
        })
    },
    onShowCart: function (e) {
        wx.navigateTo({
            url: `/pages/cart/cart?status=6`
        })
    }
});