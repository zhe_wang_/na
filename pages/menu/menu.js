
var url = require("../../utils/urls.js");
var dataManage = require("../../datamanage/apidatamanage.js");
var util = require('../../utils/util.js');
var lastClickTime = 0;
var app = getApp();
Page({
  data: {
    list: [],
    isRefresh: true,
    category_id:'0',
    showImage:false,
    image:""
  },
  onLoad: function (options) {
    this.setData({
      category_id: options.category_id
    });
    this.loadData();
  },
  onReady: function () {

  },
  onShow: function () {
    // 页面显示

  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
 
  loadData: function () {

    var context = this;
    this.showWaitDialog();
    dataManage.requestDataWithAPI("news/category/list?category_id=" + this.data.category_id, "GET", null, false, function (res) {
      if (res.code == 0) {
        context.setData({
          list: res.data
        })
      }
    }, null, function () {
      context.hideWaitDialog();
      context.setData({
        isRefresh: false,
      });
    });
  },

  onPullDownRefresh: function () {
    this.showWaitDialog();
    this.setData({
      isRefresh: true
    });
    var context = this;
    wx.showNavigationBarLoading();
    this.loadData(1);
  },

  loadMore: function () {
    this.loadData(this.data.current_page + 1);
  },
  showWaitDialog: function () {
    util.showLoadingDialog("加载中...", 10000);
  },

  hideWaitDialog: function () {
    wx.hideNavigationBarLoading();
    wx.stopPullDownRefresh();
    wx.hideToast();
  },
  itemClick: function (res) {
    if (Date.parse(new Date()) - lastClickTime < 500 && Date.parse(new Date()) - lastClickTime >= 0) {
      return;
    }

    var item = res.currentTarget.dataset.item;
    wx.navigateTo({
      url: '../detail/detail?url={0}'.format(item.link),
      complete: function () {
        lastClickTime = Date.parse(new Date());
      }
    });


  },
  onShareAppMessage: function () {
    return {
      title: "新闻",
      desc: "新闻",
      path: "/pages/menu/menu"
    }
  },

  ding: function (res) {
    var item = res.currentTarget.dataset.item;
    // this.setData({
    //   ['list[' + res.currentTarget.dataset.menu_index + '].news_list[' + res.currentTarget.dataset.index + ']ding_amount']: res.currentTarget.dataset.item.ding_amount + 1
    // })
    this.vote(1, item.id)
  },
  cai: function (res) {
    var item = res.currentTarget.dataset.item;
    // this.setData({
    //   ['list[' + res.currentTarget.dataset.menu_index + '].cai_amount']: res.currentTarget.dataset.item.cai_amount + 1
    // })
    this.vote(2, item.id)
  },
  vote: function (vote_type, news_id, cb) {
    var params = { type: vote_type };
    dataManage.requestDataWithAPI("news/event/" + news_id, "POST", params, true, function (res) {
      if (res.code == 0) {
        wx.showToast({
          title: '操作成功',
        })
        cb();
      }
    }, function (res) {
      wx.showToast({
        title: res.message,
        icon: "none"
      })
    }, function () {

    });
  },
  loadMore: function () {
    this.loadData(this.data.current_page + 1);
  },
  showWaitDialog: function () {
    util.showLoadingDialog("加载中...", 10000);
  },

  hideWaitDialog: function () {
    wx.hideNavigationBarLoading();
    wx.stopPullDownRefresh();
    wx.hideToast();
  },



  itemClick: function (res) {
    if (Date.parse(new Date()) - lastClickTime < 500 && Date.parse(new Date()) - lastClickTime >= 0) {
      return;
    }
    this.setData({
      showImage: true,
      image: res.currentTarget.dataset.item.image
    })
    var that = this
    setTimeout(function () {
      //要延时执行的代码  

      var item = res.currentTarget.dataset.item;
      wx.navigateTo({
        url: '../newslist/newslist?category_id={0}'.format(item.id),
        complete: function () {
          lastClickTime = Date.parse(new Date());
          that.setData({
            showImage: false
          })
        }
      });
    }, 1000) //延迟时间 这里是1秒  
  


  },
  newsClick: function (res) {
    if (Date.parse(new Date()) - lastClickTime < 500 && Date.parse(new Date()) - lastClickTime >= 0) {
      return;
    }

    var item = res.currentTarget.dataset.item;
    wx.navigateTo({
      url: '../detail/detail?url={0}'.format(item.link),
      complete: function () {
        lastClickTime = Date.parse(new Date());
      }
    });


  }
})