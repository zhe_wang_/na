let makeurl = require("../../utils/urls.js");
let dataManage = require("../../datamanage/apidatamanage.js");
let util = require('../../utils/util.js');
let evenBus = require('../../utils/event-bus');
let constant = require('../../utils/constant');
Page({
    data: {
        id: 0,
        images:[],
        product:{},
        usd: '$6400',
        rmb: '￥4470',
        benefit: '现金购买获赠200积分',
        num: 1,
        payWithCredit: false,
        selectedPayType: 0,
      selectedAddress: { user_name: '', address: '', country: '', phone:''},
        remark:'',
        tags:[],
        showRemarkStr:''
    },
    onLoad: function (options) {

        this.data.id = options.p_id  || 12;
      
        evenBus.on(constant.TAG_ADDRESS_SELECTED, this, data=>{
            this.addressChanged(data);
        });
        evenBus.on(constant.TAG_REMARK_SELECTED, this, data=>{
            this.remarkChanges(data);
        });
        this.loadData();
    },
    onReady: function () {
        // Do something when page ready.
    },
    onShow: function () {
        // Do something when page show.
    },
    onHide: function () {
        // Do something when page hide.
    },
    onUnload: function () {
        // Do something when page close.

    },
    onPullDownRefresh: function () {
        // Do something when pull down.
    },
    onReachBottom: function () {
        // Do something when page reach bottom.
    },
    //onShareAppMessage: function () {
    // return custom share data when user share.
    //},
    onPageScroll: function () {
        // Do something when page scroll
    },
    loadData: function () {
      dataManage.requestDataWithAPI("order/product/" + this.data.id, "GET", null, true, (res)=> {
            this.setData({
                product: res.data,
            });
      }, (res) => {
        wx.showModal({
          title: '提示',
          content: '抱歉该商品暂时无货',
          showCancel: false,
          confirmText: "确定",
          success: function (res) {
            wx.navigateBack({
              delta: 1,
            })
          }
        });
      }, null);
    },
    onNumDel: function (e) {
        if (this.data.num <= 1) {
            return;
        } else if (this.data.num < this.data.product.min_pack) {
            this.setData({
                num: this.data.product.min_pack
            });
        } else {
            this.setData({
                num: this.data.num - 1
            });
        }
    },
    onNumAdd: function (e) {
        if(this.data.num >= Math.max(this.data.product.max_pack)) {
            this.setData({
                num: this.data.product.max_pack
            })
        } else {
            this.setData({
                num: this.data.num + 1
            });
        }
    },
    onNumInput: function (e) {
        let value = e.detail.value;
        this.setData({
            num: value
        });
    },
    onToggleCreditPay: function (e) {
        this.setData({
            payWithCredit: !this.data.payWithCredit
        });
    },
    onAddressClicked: function (e) {
        wx.navigateTo({
          url: '/pages/address/address'
        });
    },
    onRemarkClicked: function (e) {
        wx.navigateTo({
            url: `/pages/remark/remark?id=${this.data.id}`
        });
    },
    addressChanged: function (item) {
        this.setData({
            selectedAddress: item
        })
    },
    remarkChanges: function (item) {
        this.setData({
            remark: item.remark,
            tags: item.tags,
            showRemarkStr: `${item.remark}  ${item.tags.join(',')}`
        })
    },
    onPayTypeClickedWechat: function (e) {
        this.setData({
            selectedPayType: 1
        })
    },

  onPayTypeClickedOffline: function (e) {
    this.setData({
      selectedPayType: 3
    })
  },
  onPayTypeClickedPoint: function (e) {
    this.setData({
      selectedPayType: 2
    })
  },
    onSubmitOrderClicked: function (e) {

        if(util.isEmpty(this.data.selectedPayType)) {
            util.showModalDialog('请选择付款方式');
            return;
        }
      if (this.data.selectedPayType == 3){
        if(util.isEmpty(this.data.selectedAddress.address)) {
            util.showModalDialog('请填写收货地址');
            return;
        }
    }
        // if(util.isEmpty(this.data.selectedPayType)) {
        //     util.showModalDialog('请填写收货地址');
        //     return;
        // }
        let params = {
            id: this.data.id,
            inputName: this.data.selectedAddress.user_name,
            inputAddress: `${this.data.selectedAddress.address} ${this.data.selectedAddress.country}`,
            inputPhone: this.data.selectedAddress.phone,
            pay_type: this.data.selectedPayType,
            num: this.data.num,
            remark: this.data.remark,
            tags: this.data.tags.join('(#)')
        };
        let that = this;
        dataManage.requestDataWithAPI("/order/checkout/", "POST", params, true,
            function (res) {
                wx.showModal({
                    title: '支付确认',
                    content: res.data.award_jifen + '\r\n' + res.data.cost_jifen + '\r\n购买数量：' + res.data.num + '\r\n支付方式：' + res.data.pay_type_desc + '\r\n价格:' + res.data.total_price + 
                      '\r\n商品备注:' + that.data.showRemarkStr +'\r\n收货人姓名:' + that.data.selectedAddress.user_name + '\r\n地址:' + that.data.selectedAddress.address +that.data.selectedAddress.country + '\r\n电话:' + that.data.selectedAddress.phone,
                    success: function (res) {
                        if (res.confirm) {
                            dataManage.requestDataWithAPI("/order/pay/", "POST", params, true,
                                function (res) {
                                  if (that.data.selectedPayType == 1){
                                    var para = res.data.tips.body.sdk_params
                                    wx.requestPayment({
                                      timeStamp: para.timeStamp,
                                      nonceStr: para.nonceStr,
                                      package: para.package,
                                      signType: para.signType,
                                      paySign: para.paySign,
                                      success(res) { 
                                        wx.showModal({
                                          title: '支付成功',
                                          showCancel: false,
                                          success: function (res) {
                                            wx.navigateBack();
                                          }
                                        })
                                      },
                                      fail(res) { }
                                    })

                                  }else{
                                    wx.showModal({
                                        title: '支付成功',
                                        showCancel:false,
                                        content: '订单编号:'+res.data.order_no, success: function (res) {
                                            wx.navigateBack();
                                        }
                                    })
                                  }
                                }, function (res2) {

                                    wx.showModal({
                                        title: '系统提示',
                                        content: res2.message,
                                        showCancel:fasel,
                                        success: function (res) {
                                            wx.hideLoading();

                                        }
                                    })
                                }, function () {
                                    wx.hideLoading();
                                });
                        } else if (res.cancel) {
                            console.log('用户点击取消')
                        }
                    }
                })
            }, function (res) {

                wx.showToast({
                    title: res.message,
                    icon: 'none',
                    duration: 1000
                })
            }, function () {
                wx.hideLoading();
            });
    }
})