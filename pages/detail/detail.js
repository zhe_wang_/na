var dataManage = require("../../datamanage/apidatamanage.js");
Page({
  
  data: {
    newUrl: 0,
    token:'',
    url:'',
    nid: -1
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数

    this.setData({
      newUrl: options.url ||
        "https://news.ninedishes.com/portal/article/index/id/100.html" + '?token=' + wx.getStorageSync('access_token'),
      url: options.url ||
        "https://news.ninedishes.com/portal/article/index/id/100.html",
      nid: options.nid || -1
    });
    if(this.data.nid != -1){
      this.setData({
        newUrl: "https://news.ninedishes.com/portal/article/index/id/" + `${this.data.nid}`+".html" + '?token=' + wx.getStorageSync('access_token'),
        url: "https://news.ninedishes.com/portal/article/index/id/" + `${this.data.nid}` + ".html"
      });
    }
    console.log("this.data.newUrl = "+this.data.newUrl)
    console.log("this.data.nid  = " + `${this.data.nid}`)
    if (dataManage.isLogin()) {
      this.loadShareToken()
      if (options.share_token) {
        dataManage.requestDataWithAPI('service/check-token', 'POST', { share_token: options.share_token }, true, res => {
          console.log(res)
        })
      }
    }
  },

  loadShareToken: function () {

    var that = this
    dataManage.requestDataWithAPI('service/gen-token', 'GET', null, true, res => {
      that.setData({
        share_token: res.data.share_token
      })
    })

  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示

    if (dataManage.isLogin()) {
      this.setData({
        newUrl: `${this.data.url}` + '?token=' + wx.getStorageSync('access_token'),
      });
    }

    console.log('reset token ' + this.data.newUrl)
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  onShareAppMessage: function () {

    console.log("this.data.share_token = " + this.data.share_token)
    return {
      title: "服务温哥华",
      desc: "服务温哥华",
      path: `/pages/detail/detail?url=${this.data.url}&share_token=${this.data.share_token}`
    }
  },
    onMessageReceived: function (e) {
        console.error(e);
    }
})