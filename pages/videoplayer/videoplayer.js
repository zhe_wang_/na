let base64 = require('../../utils/base64.js');
Page({
  data: {
    // text:"这是一个页面
    url: ""
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
      this.setData({
          url: base64.decode(options.url)
      })
  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
})