let makeurl = require("../../utils/urls.js");
let dataManage = require("../../datamanage/apidatamanage.js");
let util = require('../../utils/util.js');
let eventBus = require('../../utils/event-bus');
let constant = require('../../utils/constant');

Page({
    data: {
        menu:[
            {
                id: 'atMe',
                name: '回复我的',
                count: 0
          },
          {
            id: 'myPublish',
            name: '我发布的',
            count: 0
          },
            {
                id: 'private',
                name: '私信',
                count: 0
            },
          {
            id: 'sysmsg',
            name: '系统消息',
            count: 0
          },
            {
                id: 'filter',
                name: ' 筛选',
                count: 0
            },
        ],
        atMeMsg:{
            currentPage:0,
            hasMore: false,
            list:[]
        },
        privateMsg:{
            currentPage:0,
            hasMore: false,
            list:[]
        },
      sysMsg: {
        currentPage: 0,
        hasMore: false,
        list: []
      },
        myPublishMsg:{
            currentPage:0,
            hasMore: false,
            list:[]
        },
        filterMsg:{
            currentPage:0,
            hasMore: false,
            list:[]
        },
        bindInput: '',
        currentSelectedIndex: 0,
        scrollToView: ''
    },
    onLoad: function (options) {
        this.loadData(1);
        this.loadAtMeMsgCount();
        this.loadPrivateMsgCount();
        eventBus.on(constant.TAG_COMMENT_DELETED, this, data => {
            this.deleteMyComment(data.comment_id);
        });
    },
    onReady: function () {
        // Do something when page ready.
    },
    onShow: function () {
        // Do something when page show.
    },
    onHide: function () {
        // Do something when page hide.
    },
    onUnload: function () {
        if (this.innerAudioContext) {
            this.innerAudioContext.destroy();
            this.innerAudioContext = null;
        }
        eventBus.remove(constant.TAG_COMMENT_DELETED, this);
    },
    onPullDownRefresh: function () {
        // Do something when pull down.
    },
    onReachBottom: function () {
        // Do something when page reach bottom.
    },
    //onShareAppMessage: function () {
    // return custom share data when user share.
    //},
    onPageScroll: function () {
        // Do something when page scroll
    },

    onMenuItemClicked: function (e) {
        let index = e.currentTarget.dataset.index;
        this.onMenuItemChanged(index);
    },

    onSwiperChanged: function(e) {
        let index = e.detail.current;
        if (index === this.data.currentSelectedIndex) {
            return;
        }
        this.onMenuItemChanged(index);
    },

    onMenuItemChanged: function (index) {
      console.log("index =" + index);
        let message = this.data.atMeMsg;
        
        if (index == 1) {
            message = this.data.privateMsg;
        } else if (index == 2) {
          message = this.data.sysMsg;
        } 
        else if (index == 3) {
          message = this.data.myPublishMsg;
        }else if (index == 4) {
            message = this.data.filterMsg;
        }
        if (index > 0) {
            this.data.scrollToView = `menu_${index - 1}`;
        }
        this.setData({
            currentSelectedIndex: index,
            scrollToView: this.data.scrollToView
        });
        if (message.currentPage <= 0) {
            this.loadData(1);
        }
    },

    loadData(page) {
        let type = this.data.menu[this.data.currentSelectedIndex].id;


        let param = {
            page: page
        };

      var url = "comment/news/0/list"
      if (type == 'sysmsg'){
       var url = "service/message"
      }
        if (type === 'atMe') {
            param['is_repay_me'] = 1;
        }
        if (type === 'private') {
            param['is_private'] = 1;
        }
        if (type === 'myPublish') {
            param['is_my'] = 1;
        }
        if (type === 'filter') {
            if (util.isEmpty(this.data.bindInput)) {
                return;
            }
            param['keyword'] = this.data.bindInput;
        }
      dataManage.requestDataWithAPI(url, 'GET', param, true, res=>{
            let {current_page, has_next_page, data} = res.data;
            let message = this.data.atMeMsg;
            if (type == 'private') {
                message = this.data.privateMsg;
            } else if (type == 'myPublish') {
                message = this.data.myPublishMsg
            } else if (type == 'sysmsg') {
              message = this.data.sysMsg
            } else if (type == 'filter') {
                message = this.data.filterMsg;
            }
            message.currentPage = current_page;
            message.hasMore = has_next_page;
            message.list = current_page == 1 ? data : [...message.list, ...data];
            if (type == 'atMe') {
                this.setData({
                    atMeMsg: message
                });
            } else if (type == 'myPublish') {
                this.setData({
                    myPublishMsg: message
                });
            } else if (type == 'private') {
                this.setData({
                    privateMsg: message
                });
            } else if (type == 'sysmsg') {
              this.setData({
                sysMsg: message
              });
            }  else if (type == 'filter') {
                this.setData({
                    filterMsg: message
                });
            }

        })
    },
    bindInput: function(e) {
        this.data.bindInput = e.detail.value;
    },
    search: function () {
        this.loadData(1);
    },

    onScrollTop: function(e) {
        this.loadData(1);
    },
    onScrollBottom: function(e) {
        let type = this.data.menu[this.data.currentSelectedIndex].id;
        let message = this.data.atMeMsg;
        if (type == 'myPublish') {
            message = this.data.myPublishMsg;
        } else if (type == 'private') {
            message = this.data.privateMsg;
        } else if (type == 'sysmsg') {
          message = this.data.sysMsg;
        }else if (type == 'filter') {
            message = this.data.filterMsg;
        }
        if (message.list.length > 0 && message.hasMore) {
            this.loadData(message.currentPage + 1);
        }
    },

    onVoiceClicked: function (e) {
        let {start, id, parentId, audioPath} = e.detail;
        if (start) {
            this.playAudio(id, audioPath);
        } else {
            this.stopAudio(id, audioPath);
        }
    },
    playAudio: function (id, audioPath) {
        if (!audioPath) {
            return
        }
        if (this.innerAudioContext) {
            this.innerAudioContext.stop();
        }
        eventBus.emit(constant.TAG_STOP_VOICE, {currentId: id});
        this.innerAudioContext = wx.createInnerAudioContext();
        this.innerAudioContext.onPlay(() => {
            console.log('开始播放');
        });
        this.innerAudioContext.onError((res) => {
            console.log('播放出错');
            console.log(res);
            eventBus.emit(constant.TAG_STOP_VOICE, {currentId: 0});
        });
        this.innerAudioContext.onEnded((res) => {
            console.log('播放结束');
            console.log(res);
            eventBus.emit(constant.TAG_STOP_VOICE, {currentId: 0});
        });
        this.innerAudioContext.src = audioPath; // 这里可以是录音的临时路径
        this.innerAudioContext.play();
    },

    stopAudio: function (id, audioPath) {
        if (this.innerAudioContext) {
            // this.innerAudioContext.stop();
            this.innerAudioContext.destroy();
            eventBus.emit(constant.TAG_STOP_VOICE, {currentId: id});
        }
    },
    deleteMyComment: function (commentId) {
        let atMeMsg = this.data.atMeMsg;
        let privateMsg = this.data.privateMsg;
        let myPublishMsg = this.data.myPublishMsg;
        let filterMsg = this.data.filterMsg;

        for (let i = 0; i < atMeMsg.list.length; i++) {
            if(atMeMsg.list[i].id == commentId) {
                atMeMsg.list.splice(i, 1);
                break;
            }
        }
        for (let i = 0; i < privateMsg.list.length; i++) {
            if(privateMsg.list[i].id == commentId) {
                privateMsg.list.splice(i, 1);
                break;
            }
        }
        for (let i = 0; i < myPublishMsg.list.length; i++) {
            if(myPublishMsg.list[i].id == commentId) {
                myPublishMsg.list.splice(i, 1);
                break;
            }
        }
        for (let i = 0; i < filterMsg.list.length; i++) {
            if(filterMsg.list[i].id == commentId) {
                filterMsg.list.splice(i, 1);
                break;
            }
        }
        this.setData({
            atMeMsg: atMeMsg,
            privateMsg: privateMsg,
            myPublishMsg: myPublishMsg,
            filterMsg: filterMsg
        });
    },
    loadAtMeMsgCount: function () {
        dataManage.requestDataWithAPI('comment/atmy', 'GET', {}, true, res=>{
            this.data.menu[0].count = res.data.unread;
            this.setData({
                menu: this.data.menu
            });
        }, err=>{});
    },
    loadPrivateMsgCount: function () {
        dataManage.requestDataWithAPI('comment/personalCount', 'GET', {}, true, res=>{
            this.data.menu[1].count = res.data.unread;
            this.setData({
                menu: this.data.menu
            });
        }, err=>{});
    },
  onMoreClicked: function (e) {
    wx.navigateTo({
      url: '/pages/category/category'
    })
  },

})