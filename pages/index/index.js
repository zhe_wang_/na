//index.js
//获取应用实例
var url = require("../../utils/urls.js");
var dataManage = require("../../datamanage/apidatamanage.js");
var util = require('../../utils/util.js');
var lastClickTime = 0;
var app = getApp();
import LruCach from '../../utils/LRUCache';

Page({
  data: {
    id: 1,
    bindInput: "",
    category_id: '',
    list: [],
    initialMenuId: '',
    isRefresh: true,
    showLoadMoreLoading: false,
    total: 0,
    reply_me: 0,
    carts: 0,
    showNear:false,

    showToday: false,
    scrollMenu: [],
    showFunctionBtn:true,
    latitude:0,
    longitude:0,
    baseMenu: [],
    endMenu:[],
    currentSelectedIndex: 0,
    data: {},
    filters_name:'全部',
    filters_id:0,
    filters_main: '',
    filters: [],
    scrollToView: 'menu_0',
    scrollToSearchOnce:false,
    scrollToSearch: '',
    searchTag: "",
    search: false,

    long_filters: [],
    filterMenuItem: { //我这里是写死的 index和 data ,如果后面这个分类是动态获取的可以遍历一下在获取 index 和 data
      index: 3,
      data: {
        id: 'filter',
        name: '筛选'
      }
    },
    filterMenuItemPos: 0, //0不显示， 1左边， 2右边
    filterMenuItemIndex: 3,
  },
  lruCache: new LruCach(3), //防止分类页面过多导致页面卡顿(对每个页面的 item 进行操作 例如收藏点赞啥的 需要同步更新到缓存)

  setDefaultValue() {
    this.setData({
      id: 1,
      category_id: 'new:' + 1,
    })
  },
  bindInput: function(e) {
    this.setData({
      bindInput: e.detail.value
    })
  },
  onLoad: function(options) {
    const updateManager = wx.getUpdateManager()

    updateManager.onCheckForUpdate(function (res) {
      // 请求完新版本信息的回调
      console.log(res.hasUpdate)
    })

    updateManager.onUpdateReady(function () {
      wx.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        success(res) {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      })
    })
    //根据show_point显示红色的未读标识
    // wx.getStorage('show_point')
    if (!options.id) {
      try {
        var value = wx.getStorageSync('category_id')
        if (value) {
          // Do something with return value
          this.setData({
            id: value,
            category_id: 'new:' + value,
          })
        } else {
          this.setDefaultValue()
        }
      } catch (e) {
        this.setDefaultValue()
      }

    } else {
      this.setData({
        id: options.id,
        category_id: 'new:' + options.id,
        initialMenuId: options.initialMenuId
      })
    }
    this.getUserCounts(function() {});
    this.loadData(this.data.category_id);
  },

  getUserCounts: function(cb) {
    var context = this;

    if (dataManage.isLogin()) {
    dataManage.requestDataWithAPI("/user/stats", "POST", null, true, function(res) {
      if (res.code == 0) {
        context.setData({
          carts: res.data.carts,
          reply_me: res.data.reply_me
        })
      }
    }, function(res) {}, function() {

    });
    }
  },
  showWaitDialog: function() {
    util.showLoadingDialog("加载中...", 10000);
  },
  //用户点击了我附近的按钮

  showMsg: function (e) {
    wx.navigateTo({
      url: '/pages/mymessages/mymessages'
    })
  },


  showToday: function (e) {
    this.setData({
      showToday: !this.data.showToday,
    })
  },
  showNear: function (e) {
    this.setData({
      showNear: !this.data.showNear,
    })
  },

  getLocation: function (e) {
    var that = this;
    wx.getSetting({
      success(res) {
        if (res.authSetting['scope.userLocation']) {
          that.search(e);
        } else {

          wx.getLocation({
            type: 'wgs84',
            success(res) {
              that.setData({
                latitude: res.latitude,
                longitude: res.longitude
              })

              that.search(e);

            }
          })



          wx.getSetting({
            success(res) {
              console.log(res.authSetting)
              if (!res.authSetting['scope.userLocation']) {
                wx.openSetting({
                  success(res) {
                    console.log(res.authSetting)

                  }
                })
              }
            }
          })
        }
      }
    })
  },
   
  onShowCart: function (e) {
    wx.navigateTo({
      url: `/pages/cart/cart?status=6`
    })
  },
  loadDataFromAPI: function(id, page) {
    this.showWaitDialog();
    var context = this;
    let [preId, postId] = `${id}`.split(':');
    var hot = ''
    if (preId == 'hot') {
      hot = '1'
    } else {
      hot = '0'
    }
    var keyword = ''
    if (this.data.search) {
      keyword = this.data.searchTag
    }
    if (preId == 'filter') {
      postId = context.data.filters_id
    } 
    var recommend = ''
    if (preId == 'filter' && this.data.searchTag.length == 0) {
      recommend = '1'
    } else {
      recommend = '0'
    }


    var _lat = 0
    var _lng = 0
    if(this.data.showNear){
      _lat = this.data.latitude
      _lng = this.data.longitude
    }
    var params = {
      page: page,
      hot: hot,
      lat: context.data.latitude,
      lng: context.data.longitude,
      category_id: postId ? postId : preId,
      keyword: keyword,
      recommend: recommend
    };
    
    dataManage.requestDataWithAPI("news/list", "GET", params, false, function(res) {

      if (res.code == 0) {
        if (context.data.scrollMenu.length <= 0) {

          context.setData({
            filters_main: res.data.category_name,
            baseMenu: [{
                id: 'new:' + context.data.id,
                name: '最新',
                filters: res.data.filters,

              long_filters: res.data.long_filters,
                main: true
              },
              {
                id: 'hot:' + context.data.id,
                name: '最热',
                filters: res.data.filters,

                long_filters: res.data.long_filters,
                main: true
              },
              {
                id: 'filter:' + context.data.id,
                name: '筛选',
                filters: res.data.filters,

                long_filters: res.data.long_filters,
                type: 'filter'
              },
            ],
            endMenu: [{
              id: 0,
              name: '亲到底了',
              filters: [],
              long_filters: []
            }]

          })

          var menu = context.data.baseMenu.concat(res.data.sub_category).concat(context.data.endMenu)
          // menu[0].id = `new:${id}`;
          // menu[1].id = `hot:${id}`;
          context.setData({
            scrollMenu: menu,
          })
        }
        // context.setData( {
        //   page: page,
        //   scrollMenu: menu,
        //   maxPage: res.data.total,
        //   list: page === 1 ? res.data.data : [...res.data.data[id].list, ...res.data.data]
        // })

        //缓存模块

        let lruCacheName = context.getLRUCacheName(id);
        context.data.data[lruCacheName] = {
          page: page,
          maxPage: res.data.total,
          list: page === 1 ? res.data.data : [...context.data.data[lruCacheName].list, ...res.data.data]
        };
        let removed = context.lruCache.put(lruCacheName, context.data.data[lruCacheName]);

        if (removed) {
          context.data.data[removed.key] = undefined;
        }

        context.setData({
          data: context.data.data
        })


        //如果是搜索的话要直接滚动到第一条
        if (context.data.search && context.data.scrollToSearchOnce) {
          console.log("搜索结果");
          context.setData({
            scrollToSearch: `search_0`,
            scrollToSearchOnce:false
          });
        }
        //用于分类页面直接选中分类进入
        if (context.data.initialMenuId && context.data.initialMenuId != '0') {
          for (let i = 0; i < context.data.scrollMenu.length; ++i) {
            if (context.data.scrollMenu[i].id == context.data.initialMenuId) {
              context.changeScrollViewSelectedView(i)
              context.setData({
                initialMenuId: ''
              })
              break;
              
            }
          }

        }
      }
    }, null, function() {
      context.hideWaitDialog();

      let lruCacheName = context.getLRUCacheName(id);
      if (context.data.data[lruCacheName].list.length == 0 && context.getFilterMenuIndex() != context.data.currentSelectedIndex){
      wx.showToast({
        title: '内容正在建设中',
        icon: 'none',
        duration: 2000
      })
    }
      context.setData({
        isRefresh: false,
        showLoadMoreLoading: false
      });
    });
  },
  getLRUCacheName: function(id) {
    for (let i = 0; i < this.data.scrollMenu.length; i++) {
      if (id == this.data.scrollMenu[i].id) {
        return this.data.scrollMenu[i].name;
      }
    }
  },

  hideWaitDialog: function() {
    wx.hideNavigationBarLoading();
    wx.stopPullDownRefresh();
    wx.hideToast();
  },
  onPullDownRefresh: function() {
    console.error('aaaaa');
  },
  onReachBottom: function() {
    console.error('bbbbbb');
  },


  onSwiperChanged: function(e) {
    let index = e.detail.current;
    if (index === this.data.currentSelectedIndex) {
      return;
    } if (index == this.data.scrollMenu.length - 1) { //最后一个不处理

      wx.showToast({
        title: '亲，到底了',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    // this.setData({
    //     currentSelectedIndex: index
    // });
    // this.loadData(this.data.id);
    // this.setData({
    //     scrollMenu: this.getFilterMenuDynamic(index)
    // });
    this.changeScrollViewSelectedView(index);
  },
  onMenuItemClicked: function(e) {
    let index = e.currentTarget.dataset.index;
    if (index === this.data.currentSelectedIndex) {
      return;
    } 
    if (index == this.data.scrollMenu.length - 1) { //最后一个不处理
      wx.showToast({
        title: '亲，到底了',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    // this.setData({
    //     scrollMenu: this.getFilterMenuDynamic(index)
    // });
    this.changeScrollViewSelectedView(index);
    // this.setData({
    //     currentSelectedIndex: index
    // });
    // this.loadData(this.data.id);
  },

  changeScrollViewSelectedView: function(curIndex) {

    let selectedIndex = curIndex;
    
    let filterIndex = this.getFilterMenuIndex();
    if (curIndex > 0) {
      selectedIndex = curIndex - 1;
    }
    this.setData({
      scrollMenu: this.getFilterMenuDynamic(curIndex),
      scrollToView: `menu_${selectedIndex}`,
      currentSelectedIndex: curIndex
      // filterMenuItemPos: this.getFilterMenuItemPos(curIndex)
    });
    //设置给筛选页面使用的id

    if (curIndex != filterIndex) { //如果当前选中的就是不是筛选, 刷新filters，用于下次选中时的tag

      this.setData({
        search:false,
        filters: this.data.scrollMenu[curIndex].filters,
        long_filters: this.data.scrollMenu[curIndex].long_filters,
      });

      if (!this.data.scrollMenu[curIndex].main){
        this.setData({
          filters_name: this.data.filters_main + "·" + this.data.scrollMenu[curIndex].name,
          filters_id: this.data.scrollMenu[curIndex].id,
        });

      }else{
        this.setData({
          filters_name: this.data.filters_main,
          filters_id: this.data.id,
        });
      }

      //2019-02-01 00:01:25 要求所有界面都显示 showFunctionBtn
      this.setData({ //如果index < 2 那么就在筛选当中显示功能按键
        // showFunctionBtn: curIndex < 2,
      });
      for (let i = 0; i < this.data.scrollMenu[curIndex].filters.length; ++i) {
        this.data.scrollMenu[curIndex].filters[i].check = false
      }
      for (let i = 0; i < this.data.scrollMenu[curIndex].long_filters.length; ++i) {
        this.data.scrollMenu[curIndex].long_filters[i].check = false
      }

      this.loadData(this.data.scrollMenu[curIndex].id);
    }else{
      this.setData({
        bindInput:''
      })
      this.onScrollTop();
    }
  },
  search: function() {

    
    var tags = [];

    //循环filters保存到tags
    for (let i = 0; i < this.data.filters.length; ++i) {
      if (this.data.filters[i].check) {
        tags.push(this.data.filters[i].name);
      }
    }

    for (let i = 0; i < this.data.long_filters.length; ++i) {
      if (this.data.long_filters[i].check) {
        tags.push(this.data.long_filters[i].name);
      }
    }
    if (this.data.showToday) {
      //获取当前时间戳
      var timestamp = Date.parse(new Date());
      timestamp = timestamp / 1000;
      console.log("当前时间戳为：" + timestamp);

      //获取当前时间
      var n = timestamp * 1000;
      var date = new Date(n);
      //年
      var Y_today = date.getFullYear();
      //月
      var M_today = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
      //日
      var D_today = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
      //时

      var today = Y_today + "-" + M_today + "-" + D_today
      var tomorrow_timetamp = timestamp + 24 * 60 * 60;
      //加一天的时间：
      var n_to = tomorrow_timetamp * 1000;
      var tomorrow_date = new Date(n_to);
      //加一天后的年份
      var Y_tomorrow = tomorrow_date.getFullYear();
      //加一天后的月份
      var M_tomorrow = (tomorrow_date.getMonth() + 1 < 10 ? '0' + (tomorrow_date.getMonth() + 1) : tomorrow_date.getMonth() + 1);
      //加一天后的日期
      var D_tomorrow = tomorrow_date.getDate() < 10 ? '0' + tomorrow_date.getDate() : tomorrow_date.getDate();

      var tomorrow = Y_tomorrow + "-" + M_tomorrow + "-" + D_tomorrow
      tags.push(today);
      tags.push(tomorrow);
    }

    if (this.data.bindInput != "") {
      tags.push(this.data.bindInput)

    }
    if (tags.length != 0) {
      this.setData({
        searchTag: tags.join(),
        search:true,
        scrollToSearchOnce: true
      });
      this.refresh(this.data.scrollMenu[this.data.currentSelectedIndex].id);
      console.log(this.data.scrollMenu[this.data.currentSelectedIndex].id)
      
    } else {
      wx.showToast({
        title: '请输入关键词',
        icon: 'none',
        duration: 2000
      })
    
  }
},
  longTagClicked: function (e) {
    var name = e.currentTarget.dataset.name;

    for (let i = 0; i < this.data.long_filters.length; ++i) {
      if (name == this.data.long_filters[i].name) {
        this.data.long_filters[i].check = !this.data.long_filters[i].check
      }
    }
    this.setData({
      long_filters: this.data.long_filters
    });
  },

tagClicked: function(e) {
  var name = e.currentTarget.dataset.name;

  for (let i = 0; i < this.data.filters.length; ++i) {
    if (name == this.data.filters[i].name) {
      this.data.filters[i].check = !this.data.filters[i].check
    }
  }
  this.setData({
    filters: this.data.filters
  });
},
getFilterMenuItemPos: function(curIndex) {
  if ((curIndex === 0 || curIndex === 1) && this.data.filterMenuItem.index >= 3) {
    return 2;
  } else if (curIndex > this.data.filterMenuItem.index + 1) {
    return 1;
  } else if (curIndex < this.data.filterMenuItemPos.index - 3) {
    return 2;
  } else {
    return 0;
  }
},

getFilterMenuIndex: function() {
  for (let i = 0; i < this.data.scrollMenu.length; i++) {
    if ('filter:' + this.data.id == this.data.scrollMenu[i].id) {
      return i;
    }
  }
  return -1;
},


getFilterMenuDynamic: function(curIndex) {
  let filterIndex = this.getFilterMenuIndex();
  let scrollMenu = this.data.scrollMenu;
  
  if (scrollMenu.length < 4) { //3个以内不用处理
    return scrollMenu;
  }
  if (curIndex == filterIndex) { //如果当前选中的就是筛选, 也不做处理
    return scrollMenu;
  }
  let filterItem = scrollMenu[filterIndex];
  if (curIndex == 0 || curIndex == 1) {
    scrollMenu.splice(2, 0, filterItem);
    scrollMenu.splice(filterIndex + 1, 1);
    return scrollMenu;
  }

  if (curIndex > this.data.currentSelectedIndex) { //向左滑=>其实菜单是往右变化的
    if (curIndex == scrollMenu.length - 1 || curIndex == scrollMenu.length - 2) {
      scrollMenu.splice(scrollMenu.length - 2, 0, filterItem);
      scrollMenu.splice(filterIndex, 1);
    } else {
      scrollMenu.splice(curIndex, 0, filterItem);
      scrollMenu.splice(filterIndex, 1);
    }

  } else { //向右滑
    if (curIndex == scrollMenu.length - 1 || curIndex == scrollMenu.length - 2) {
      return scrollMenu;
    } else {
      scrollMenu.splice(curIndex + 1, 0, filterItem);
      scrollMenu.splice(filterIndex + 1, 1);
    }
  }

  return scrollMenu;

},

loadData: function(id) {
  let lruCacheName = this.getLRUCacheName(id);
  if (!this.data.data[lruCacheName]) {
    this.loadDataFromAPI(id, 1);
  } else {
    this.lruCache.put(lruCacheName, this.data.data[lruCacheName]);
  }
},

loadMore: function(id) {
  let lruCacheName = this.getLRUCacheName(id);
  let node = this.data.data[lruCacheName];
  if (node.page && node.maxPage && node.page < node.maxPage) {
    this.loadDataFromAPI(id, node.page + 1);
  }
},

refresh: function(id) {
  this.loadDataFromAPI(id, 1);
},

onScrollTop: function(e) {
  this.refresh(this.data.scrollMenu[this.data.currentSelectedIndex].id);
},
onScrollBottom: function(e) {
  let id = this.data.scrollMenu[this.data.currentSelectedIndex].id;
  let name = this.data.scrollMenu[this.data.currentSelectedIndex].name;
  if (!this.data.data[name]) {
    return;
  }
  this.loadMore(id);
},

//点击切换分类
onFilterMenuClicked: function(e) {
  this.changeScrollViewSelectedView(this.data.filterMenuItem.index);
},

onMoreClicked: function(e) {
  wx.navigateTo({ 
    url: '/pages/category/category'
  })
},
  onShareClicked: function(e) {
    console.log('1111111111111')
  },



  onShareAppMessage: (res) => {
    return {
      title: res.target.dataset.title,
      path: '/pages/detail/detail?url='+res.target.dataset.share,
      imageUrl: res.target.dataset.image,
      success: (res) => {
        console.log("转发成功", res);
      },
      fail: (res) => {
        console.log("转发失败", res);
      }
    }
  }
})