let makeurl = require("../../utils/urls.js");
let dataManage = require("../../datamanage/apidatamanage.js");
let util = require('../../utils/util.js');
let evenBus = require('../../utils/event-bus');
let constant = require('../../utils/constant');

Page({
    data: {
        menu:[
            {
                id: 'latest',
                name: '最新',
                count: 0
            },
            {
                id: 'hot',
                name: '最热',
                count: 0
            },
            {
                id: 'filter',
                name: ' 筛选',
                count: 0
            },
        ],
      filters: [{ name: '我的名片' }, { name: '名片' }, { name: '地产' }, { name: "医生" }, { name: '汽车' }, { name: "保险" }, { name: "旅游" }, { name: "会计" }, { name: "律师" }, { name: "教育" }, { name: "餐厅" },{ name: "建筑" },],
        latest:{
            currentPage:0,
            hasMore: false,
            list:[]
        },
        hot:{
            currentPage:0,
            hasMore: false,
            list:[]
        },
        filter:{
            currentPage:0,
            hasMore: false,
            list:[]
        },
        bindInput: '',
        currentSelectedIndex: 0,
    },
    onLoad: function (options) {
        this.loadData(1);
    },
    onReady: function () {
        // Do something when page ready.
    },
    onShow: function () {
        // Do something when page show.
    },
    onHide: function () {
        // Do something when page hide.
    },
    onUnload: function () {
        // Do something when page close.
    },
    onPullDownRefresh: function () {
        // Do something when pull down.
    },
    onReachBottom: function () {
        // Do something when page reach bottom.
    },
    //onShareAppMessage: function () {
    // return custom share data when user share.
    //},
    onPageScroll: function () {
        // Do something when page scroll
    },

    onMenuItemClicked: function (e) {
        let index = e.currentTarget.dataset.index;
        this.onMenuItemChanged(index);
    },

    onSwiperChanged: function(e) {
        let index = e.detail.current;
        if (index === this.data.currentSelectedIndex) {
            return;
        }
        this.onMenuItemChanged(index);
    },

    onMenuItemChanged: function (index) {
        let favorites = this.data.latest;
        if (index == 1) {
            favorites = this.data.hot;
        } else if (index == 2) {
            favorites = this.data.filter;
        }
        this.setData({
            currentSelectedIndex: index
        });
        if (favorites.currentPage <= 0) {
            this.loadData(1);
        }
    },
  tagClicked: function (e) {
    var name = e.currentTarget.dataset.name;

    for (let i = 0; i < this.data.filters.length; ++i) {
      if (name == this.data.filters[i].name) {
        this.data.filters[i].check = !this.data.filters[i].check
      }
    }
    this.setData({
      filters: this.data.filters
    });
  },
    loadData(page) {
        let type = this.data.menu[this.data.currentSelectedIndex].id;
        let param = {
            page: page
        };
        if (type === 'hot') {
            param['hot'] = 1;
        }
        if (type === 'latest') {
            param['latest'] = 1;
        }
        if (type === 'filter') {
          if (util.isEmpty(this.data.searchTag)) {
                return;
            }
          param['keyword'] = this.data.searchTag;
        }
        dataManage.requestDataWithAPI('news/like/list', 'GET', param, true, res=>{
            let {current_page, has_next_page, data} = res.data;
            let favorites = this.data.latest;
            if (type == 'hot') {
                favorites = this.data.hot;
            } else if (type == 'filter') {
                favorites = this.data.filter;
            }
            favorites.currentPage = current_page;
            favorites.hasMore = has_next_page;
            favorites.list = current_page == 1 ? data : [...favorites.list, ...data];
            if (type === 'hot') {
                this.setData({
                    hot: favorites
                });
            } else if (type === 'latest') {
                this.setData({
                    latest: favorites
                });
            } else if (type === 'filter') {
                this.setData({
                    filter: favorites
                });
            }

        })
    },
    bindInput: function(e) {
        this.data.bindInput = e.detail.value;
    },
  search: function () {
    var tags = [];

    //循环filters保存到tags
    for (let i = 0; i < this.data.filters.length; ++i) {
      if (this.data.filters[i].check) {
        if (this.data.filters[i].name == '我的名片'){
          tags.push(wx.getStorageSync('uname'));
        }else{
        tags.push(this.data.filters[i].name);
        }
      }
    }

    if (this.data.bindInput != "") {
      tags.push(this.data.bindInput)

    }
    if (tags.length != 0) {
      this.setData({
        searchTag: tags.join(),
        search: true
      });
      this.loadData(1);


    } else {
      wx.showToast({
        title: '请输入关键词',
        icon: 'none',
        duration: 2000
      })

    }
  },
    onScrollTop: function(e) {
        this.loadData(1);
    },
    onScrollBottom: function(e) {
        let type = this.data.menu[this.data.currentSelectedIndex].id;
        let favorites = this.data.latest;
        if (type === 'hot') {
            favorites = this.data.hot;
        } else if (type === 'filter') {
            favorites = this.data.filter;
        }
        if (favorites.list.length > 0 && favorites.hasMore) {
            this.loadData(favorites.currentPage + 1);
        }

    },
    onFavoriteDeleted: function (e) {
        let newsId = e.detail.newsId;
        let latest = this.data.latest;
        let hot = this.data.hot;
        let filter = this.data.filter;

        for (let i = 0; i < latest.list.length; i++) {
            if(latest.list[i].id == newsId) {
                latest.list.splice(i, 1);
                break;
            }
        }
        for (let i = 0; i < hot.list.length; i++) {
            if(hot.list[i].id == newsId) {
                hot.list.splice(i, 1);
                break;
            }
        }
        for (let i = 0; i < filter.list.length; i++) {
            if(filter.list[i].id == newsId) {
                filter.list.splice(i, 1);
                break;
            }
        }
        this.setData({
            latest: latest,
            hot: hot,
            filter: filter
        });

    },

onMoreClicked: function(e) {
  wx.navigateTo({ 
    url: '/pages/category/category'
  })
},


  onShareAppMessage: (res) => {
    return {
      title: res.target.dataset.title,
      path: '/pages/detail/detail?url=' + res.target.dataset.share,
      imageUrl: res.target.dataset.image,
      success: (res) => {
        console.log("转发成功", res);
      },
      fail: (res) => {
        console.log("转发失败", res);
      }
    }
  }
})