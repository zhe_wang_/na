//app.js
var url = require("utils/urls.js");
var dataManage = require("datamanage/apidatamanage.js");
App({
  onLaunch: function () {
    this.build();
    this.deviceMsg();
    //首次加载显示红点，点击之后红点消失。
    wx.setStorage({
      key: 'show_point',
      data: true
    })
    var access_token = wx.getStorageSync('access_token');
    if (access_token == null || access_token == "") {

    }else{
      //只有登录的用户才统计信息
    wx.getLocation({
      type: 'wgs84',
      success(res) {
        var params = {
          lat : res.latitude,
          lng: res.longitude };

        dataManage.requestDataWithAPI("auth/login-log", "POST", params, true, function (res) {
          if (res.code == 0) {
            console.log("auth/login-log succeed");
          }
        }, function (res) {
          console.log("auth/login-log fail");

        }, function () {
          
        });
      }
    })
    }
  },
      
    deviceMsg: function () {
        wx.getSystemInfo({
            success: (res)=> {
                this.globalData.windowHeight = res.windowHeight;
                this.globalData.windowWidth = res.windowWidth;
                console.log(this.globalData);
            }
        })
    },
  getUserInfo: function (cb) {
    var that = this;
    var access_token = null;
    if (this.globalData.access_token) {
      typeof cb == "function" && cb(this.globalData.access_token)
    } else {
      //调用登录接口
      wx.login({
        success: function (loginRes) {
          if (loginRes.code) {
            console.log('登录成功！' + loginRes.code)
            var code = loginRes.code
            wx.getUserInfo({
              success: function (res) {

              }
            })

          }

        }
      });
    }
  },

  globalData: {
    userInfo: null,
    access_token: null
  },
  //用于格式化字符串
  build: function () {
    (function () {
      'use strict'
      /**
      * repository: https://github.com/yarrem/stringFormat.js 
      * Formats string in c#-like way.
      * Example: '{0} {1}'.format('Hello', 'world')
      * @this {String}
      * @param {any} Arguments that string will be replaced with. Each {n} will be raplaced with n-th argument
      * @return {string} Formated string.
      */
      String.prototype.format = function () {
        var str = this.valueOf()
          , cons = console && console.warn; // for IE8
        if (!arguments.length) {
          if (cons) {
            console.warn(str + '.format(args[]) was invoked without any arguments');
          }
          return str;
        }
        var tokens = str.match(/{\d+}/gi)
          , i = 0
          , l = tokens.length;
        for (; i < l; i++) {
          var token = tokens[i]
            , index = token.match(/\d+/);
          if (index >= arguments.length) {
            if (cons) {
              console.warn('"' + this.valueOf()
                + '".format(args[]): there is no argument for token: '
                + token + '. You may missed it');
            }
            continue;
          }
          str = str.replace(token, arguments[index]);
        }
        return str;
      }
      String.prototype.startWith = function (str) {
        if (str == null || str == "" || this.length == 0 || str.length > this.length)
          return false;
        if (this.substr(0, str.length) == str)
          return true;
        else
          return false;
        return true;
      }
      String.prototype.endWith = function (str) {
        if (str == null || str == "" || this.length == 0 || str.length > this.length)
          return false;
        if (this.substring(this.length - str.length) == str)
          return true;
        else
          return false;
        return true;
      }
    })();
  }
})