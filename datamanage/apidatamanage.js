var urls = require("../utils/urls.js");
var util = require("../utils/util.js");
var constant = require("../utils/constant.js");


function isLogin(){
  var access_token = wx.getStorageSync('access_token');
  if (access_token == null || access_token == "") {
    return false
  }
  return true
}
/**
 * 请求数据
 */
function requestDataWithAPI(apiUrl, methodName, params, withToken, onSuccess, onFail, onComplete) {
  var targetUrl = urls.makeAPIUrls(apiUrl);
  var app = getApp();
  var header = getCommonHeader(methodName) || {};
  if(withToken) {
      var access_token = wx.getStorageSync('access_token');
      if (access_token == null || access_token == "") {
        wx.navigateTo({
          url: '../usercenter/usercenter',
        })
        return
      }
      header["news-access-token"] = access_token;
      realRequset(targetUrl, methodName, params, header, onSuccess, onFail, onComplete);
    
  } else {
    realRequset(targetUrl, methodName, params, header, onSuccess, onFail, onComplete);
  }
}

function realRequset(targetUrl, methodName, params, header, onSuccess, onFail, onComplete) {
  wx.request({
    url: targetUrl,
    data: params,
    method: methodName, // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
    header: header, // 设置请求的 header
    success: function (res) {
      if (typeof (onSuccess) == "function") {
        if (res.statusCode == 200 && res.data.code == 0) {
          onSuccess(res.data);
        } else {
          if (typeof (onFail) == "function") {
            onFail(res.data);
          } else {
            if(res.statusCode == 200 && res.data.code != 0) {
              wx.showModal({
                title: '提示',
                content: res.data.msg,
                showCancel: false,
                confirmText: "确定",
                success: function (res) {

                }
              });
            } else {
              wx.showModal({
                title: '提示',
                content: "获取数据失败",
                showCancel: false,
                confirmText: "确定",
                success: function (res) {

                }
              });
            }
          }
        }
      } else {
        wx.showModal({
          title: '提示',
          content: "获取数据失败",
          showCancel: false,
          confirmText: "确定",
          success: function (res) {

          }
        });
      }
    },
    fail: function (res) {
      if (typeof (onFail) == "function") {
        onFail(res);
      } else {
        wx.showModal({
          title: '提示',
          content: "获取数据失败",
          showCancel: false,
          confirmText: "确定",
          success: function (res) {

          }
        });
      }
    },
    complete: function () {
      if (typeof (onComplete) == "function") {
        onComplete();
      }
    }
  });
}

/**
 * 设置通用header
 */
function getCommonHeader(methodName) {
    return {
      'content-type': 'application/json'
    }
}

function uploadFiles(fileArr, success, fail) {
    let uploadFilePromises = [];
    fileArr.forEach((item, index) =>{
        let promise = new Promise((resolve, reject)=>{
            let param = {
                url: urls.makeAPIUrls('fileupload'),
                filePath: item.path,
                name: 'upfile',
                header: {...getCommonHeader(), 'news-access-token': wx.getStorageSync('access_token')},
                formData: {type: item.type},
                success: (res)=> {
                    let resJson = JSON.parse(res.data);
                    if(resJson.code == 0) {
                        resolve(resJson.data);
                    } else {
                        reject('uploadError');
                    }
                },
                fail: (res)=> {
                    reject(res);
                }
            };
            wx.uploadFile(param);
        });
        uploadFilePromises.push(promise);
    });
    Promise.all(uploadFilePromises).then((res) => {
        success(res);
    }).catch(res => {
        fail(res);
    });
}

module.exports = {
  isLogin: isLogin,
  requestDataWithAPI: requestDataWithAPI,
    uploadFiles
}