let dataManage = require("../../datamanage/apidatamanage.js");
let util = require("../../utils/util.js");
Component({

    behaviors: [],

    properties: {
        // myProperty: { // 属性名
        //     type: String, // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）
        //     value: '', // 属性初始值（可选），如果未指定则会根据类型选择一个
        //     observer: function (newVal, oldVal, changedPath) {
        //         // 属性被改变时执行的函数（可选），也可以写成在methods段中定义的方法名字符串, 如：'_propertyChange'
        //         // 通常 newVal 就是新设置的数据， oldVal 是旧数据
        //     }
        // },
        // myProperty2: String, // 简化的定义方式

        nid: Number,
        cartId: Number,
        imagePath: String,
        type: String,
        title: String,
        author: String,
        time: String,
        ding_amount: Number,
        create_time_format: String,
        msg: String,
        like: String,
        link: String,
        productId: String,
        showDeleteFavorite: {
            type: Boolean,
            value: true
        }

    },
    data: {}, // 私有数据，可用于模版渲染

    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () {
    },
    moved: function () {
    },
    detached: function () {
    },

    methods: {

        addCollection: function (news_id, cb) {
            dataManage.requestDataWithAPI("news/like/" + news_id, "POST", {event: 'add'}, true, function (res) {
                if (res.code == 0) {
                    wx.showToast({
                        title: '收藏成功',
                    })
                    cb();
                }
            }, function (res) {
                wx.showToast({
                    title: res.message,
                    icon: "none"
                })
            }, function () {

            });
        },

        vote: function (vote_type, news_id, cb) {
            var params = {
                type: vote_type
            };
            dataManage.requestDataWithAPI("news/event/" + news_id, "POST", params, true, function (res) {
                if (res.code == 0) {
                    wx.showToast({
                        title: '操作成功',
                    })
                    cb();
                }
            }, function (res) {
                wx.showToast({
                    title: res.message,
                    icon: "none"
                })
            }, function () {

            });
        },
        onAddClicked: function (e) {
            this.addCollection(this.data.nid, function () {

            })

        },
        onMsgClicked: function (e) {
            wx.navigateTo({
                url: '../commentlist/commentlist?news_id={0}'.format(this.data.nid),
            });

        },
        onShareClicked: function (e) {

        },
        onLikeClicked: function (e) {
            this.vote(1, this.data.nid, ()=> {
                let count = this.data.ding_amount + 1;
                this.setData({
                    ding_amount: count
                });
                this.triggerEvent('onLikeClicked', {newsId: this.data.nid, cartId: this.data.cartId, count: count});
            })

        },
        onItemClicked: function (e) {
            wx.navigateTo({
                url: '../detail/detail?url={0}'.format(this.data.link),
            });
        },

        onPayClicked: function () {
            wx.navigateTo({
              url: `/pages/buy/buy?p_id=${this.data.productId}`
            });
        },
        onDeleteCartClicked: function (e) {
            util.showModalDialog('是否删除当前购物车项', (res) => {
                if(res.confirm) {
                    this._deleteCart(this.data.nid, () => {
                        this.triggerEvent('onCartDeleted', {cartId: this.data.cartId})
                    });
                }
            });
        },
        _deleteCart: function (news_id, cb) {
            dataManage.requestDataWithAPI('orderhelp/del-cart','POST', {id: this.data.cartId}, true, (res)=> {
                if (res.code == 0) {
                    wx.showToast({
                        title: '操作成功',
                    });
                    cb();
                }
            }, function (res) {
                wx.showToast({
                    title: res.message,
                    icon: "none"
                })
            }, function () {

            });
        },
    }

})