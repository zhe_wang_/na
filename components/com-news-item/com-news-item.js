var dataManage = require("../../datamanage/apidatamanage.js");
var util = require("../../utils/util.js");
Component({

  behaviors: [],

  properties: {
    // myProperty: { // 属性名
    //     type: String, // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）
    //     value: '', // 属性初始值（可选），如果未指定则会根据类型选择一个
    //     observer: function (newVal, oldVal, changedPath) {
    //         // 属性被改变时执行的函数（可选），也可以写成在methods段中定义的方法名字符串, 如：'_propertyChange'
    //         // 通常 newVal 就是新设置的数据， oldVal 是旧数据
    //     }
    // },
    // myProperty2: String, // 简化的定义方式
    nid: Number,
    imagePath: String,
    type: Object,
    title: String,
    author: String,
    time: String,
    ding_amount: Number,
    create_time_format: String,
    msg: String,
    like: String,
    is_link_comment: Number,
    comment_stauts:Number,
    comment_amount: String,
    link: String,
    thumbnail:String,
    showDeleteFavorite: {
      type: Boolean,
      value: false
    }

  },
  data: {}, // 私有数据，可用于模版渲染

  // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
  attached: function() {},
  moved: function() {},
  detached: function() {},

  methods: {

    addCollection: function(news_id, cb) {
      dataManage.requestDataWithAPI("news/like/" + news_id, "POST", {
        event: 'add'
      }, true, function(res) {
        if (res.code == 0) {
          wx.showToast({
            title: '收藏成功',
          })
          cb();
        }
      }, function(res) {
        wx.showToast({
          title: res.message,
          icon: "none"
        })
      }, function() {

      });
    },

    vote: function(vote_type, news_id, cb) {
      var params = {
        type: vote_type
      };
      dataManage.requestDataWithAPI("news/event/" + news_id, "POST", params, true, function(res) {
        if (res.code == 0) {
          wx.showToast({
            title: '谢谢点赞',
          })
          cb();
        }
      }, function(res) {
        wx.showToast({
          title: res.message,
          icon: "none"
        })
      }, function() {

      });
    },
    onAddClicked: function(e) {
      this.addCollection(this.data.nid, function() {
        //callback
      })

    },
    onTypeClicked: function(e) {
      wx.navigateTo({
        url: '../index/index?id={0}&initialMenuId={1}'.format(this.data.type.fid, this.data.type.id)
      });
    },
    onMsgClicked: function(e) {
      if (this.data.comment_stauts == 0) {
        //禁止评论
      }
      else{
      wx.navigateTo({
        url: '../commentlist/commentlist?news_id={0}'.format(this.data.nid),
      });
      }
    },
    onShareClicked: function(e) {
      console.log("1111111111111")

    },
    onLikeClicked: function(e) {
      var that = this;
      this.vote(1, this.data.nid, function() {
        that.setData({
          ding_amount: that.data.ding_amount + 1
        })
      })

    },
    onItemClicked: function(e) {
      if (this.data.is_link_comment == 1){
        this.onMsgClicked();
      }else{
      wx.navigateTo({
        url: `/pages/detail/detail?url=${this.data.link}&token=${wx.getStorageSync('access_token')}`,
      });
      }
    },

    onMyButtonTap: function() {
      this.setData({
        // 更新属性和数据的方法与更新页面数据的方法类似
      })
    },
    onDeleteFavoriteClicked: function(e) {
      util.showModalDialog('是否删除当前收藏', (res) => {
        if (res.confirm) {
          this._deleteCollection(this.data.nid, () => {
            this.triggerEvent('onFavoriteDeleted', {
              newsId: this.data.nid
            })
          });
        }
      });
    },
    _deleteCollection: function(news_id, cb) {
      dataManage.requestDataWithAPI("news/like/" + news_id, "POST", {
        event: 'remove'
      }, true, function(res) {
        if (res.code == 0) {
          wx.showToast({
            title: '取消收藏成功',
          });
          cb();
        }
      }, function(res) {
        wx.showToast({
          title: res.message,
          icon: "none"
        })
      }, function() {

      });
    },
  }

})