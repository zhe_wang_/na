let dataManage = require("../../datamanage/apidatamanage.js");
let util = require("../../utils/util.js");
let eventBus = require('../../utils/event-bus');
let constant = require('../../utils/constant');
Component({

    behaviors: [],

    properties: {
        step: {
            type: Number,
            value: 0
        },
        credit: {
            type: String,
            value: '0'
        },
        creditLog: {
            type: Array,
            value: []
        },
        account: {
            type: String,
            value: ''
        },
        count: {
            type: String,
            value: '',
        },
        transferUser: {
            type: Object,
            value: {}
        },
        transferring: {
            type: Boolean,
            value: false
        }

    },
    data: {
        clause:[
            '1积分在本站内等值$0,01加元使用',
            '转发一次可获得1个积分',
            '每天每人最多转发20次',
            // '发布个人广告信息可获得1积分',
            '每天每人最多20条',
            '购买本站商品获得相应的积分具体参考商品详情',
            '参加本站活动亦可获得相应的积分具体参考活动详情'
        ]
    }, // 私有数据，可用于模版渲染

    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () {
        console.error(this.data);
    },
    moved: function () {
    },
    detached: function () {
        eventBus.emit(constant.TAG_CREDIT_DETACHED, {data: this.data});
    },

    methods: {
        onAccountInput: function (e) {
          let account = e.detail.value;
          this.data.account = account;
        },

        onCountInput: function (e) {
            let count = e.detail.value;
            this.data.count = count;
        },

        onBuyCreditClicked: function (e) {

        },
        onCreditTransferClicked: function (e) {
            let isTransferring = this.data.transferring;
            if (isTransferring) {
                this._clearData();
            } else {
                this.setData({
                    transferring: !isTransferring,
                    step: 1
                });
            }
        },
        onUseCreditClicked: function (e) {
          wx.navigateTo({
            url: '../index/index?id=152'
          });

        },
        onAccountConfirmedClicked: function (e) {
            this._getUserFromId(this.data.account);
        },
        onTransferConfirmedClicked: function (e) {
          this._transferConfirm(this.data.account, this.data.count)
        },
        _getUserFromId: function (id) {
            // user/getuser
            dataManage.requestDataWithAPI('user/getuser', 'GET', {id: id}, true, res=> {
                this.setData({
                    transferUser: res.data,
                    step: 2
                });
            });
        },
        _transferConfirm: function (id, count) {
            dataManage.requestDataWithAPI('user/attornjifen', 'POST', {id: id, jifen: count}, true, res=>{
               util.showModalDialog(res.data);
               this._clearData();
            });
        },
        _clearData: function () {
            this.setData({
                step: 0,
                credit: '',
                account:'',
                count: '',
                transferUser: {
                },
                transferring: false
            });
        }
    }

})