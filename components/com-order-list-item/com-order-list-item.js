let dataManage = require("../../datamanage/apidatamanage.js");
let util = require('../../utils/util.js');
let eventBus = require('../../utils/event-bus');
let constant = require('../../utils/constant');
Component({

    behaviors: [],

    properties: {
        orderId: String,
        imagePath: String,
        title: String,
        createTime: String,
        name: String,
        phone: String,
        orderNo: String,
        address: String,
        remark: String,
        status: String,
        statusDesc: String,
        refundApply: String
    },
    data: {}, // 私有数据，可用于模版渲染

    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () {
    },
    moved: function () {
    },
    detached: function () {
    },

    methods: {
        onRefundClicked: function (e) {
            util.showModalDialog('是否申请退款', (res) => {
                if(res.confirm) {
                    this._refund(this.data.orderId);
                }
            });

        },

        _refund: function (orderId) {
            dataManage.requestDataWithAPI(`orderhelp/appy-refund/${orderId}`, 'POST', {}, true, res=>{
                eventBus.emit(constant.TAG_ORDER_REFUND, {order_id: orderId});
            })
        }
    }

})