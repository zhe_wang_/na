var dataManage = require("../../datamanage/apidatamanage.js");
let evenBus = require('../../utils/event-bus');
let constant = require('../../utils/constant');
let util = require('../../utils/util');
let base64 = require('../../utils/base64');
Component({

    behaviors: [],

    properties: {
        // myProperty: { // 属性名
        //     type: String, // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）
        //     value: '', // 属性初始值（可选），如果未指定则会根据类型选择一个
        //     observer: function (newVal, oldVal, changedPath) {
        //         // 属性被改变时执行的函数（可选），也可以写成在methods段中定义的方法名字符串, 如：'_propertyChange'
        //         // 通常 newVal 就是新设置的数据， oldVal 是旧数据
        //     }
        // },
        // myProperty2: String, // 简化的定义方式
        comment_id:Number,
        nid: Number,
        text: String,
        media_path: Array,
        imagePath: String,
        type: String,
        time: String,
        mediaVideoPath: String,
        ding_amount: Number,
        create_time_format: String,
        msg: String,
        like: String,
        nickname: String,
        post_ding: Number,
        replyCount: Number,
        audioPath: String,
        parentId: {
            type: Number,
            value: 0
        },
        avatar: String,
        isPrivate: {
            type: Number,
            value: 0
        },
        isMy: {
            type: Boolean,
            value: false
        }

    },
    data: {
        audioRecording: false
    }, // 私有数据，可用于模版渲染

    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () {
    },
    ready: function(){
        evenBus.on(constant.TAG_STOP_VOICE,this,(data)=>{
            console.error(data);
          if (data.currentId != this.data.comment_id) {
                this.setData({
                    audioRecording: false
                });
            }
        });
        this.setData({
          imagePath:this.data.media_path[0]
        })
    },
    moved: function () {
    },
    detached: function () {
        evenBus.remove(constant.TAG_STOP_VOICE,this);
    },

    methods: {
      showimg: function (e){
        console.log(this.data.imagePath);
        wx.previewImage({
          current: this.data.imagePath, // 当前显示图片的http链接
          urls: [this.data.imagePath] // 需要预览的图片http链接列表
        })
      },
      imageIndexChanged: function (e) {
        this.setData({
          imagePath: this.data.media_path[e.detail.current]
        })
      },
        onShareClicked: function (e) {

        },
        onLikeClicked: function (e) {
            var that = this;
            this._vote(()=> {
                that.setData({
                    post_ding: that.data.post_ding + 1
                })
            })

        },
        onReplyClicked: function(res) {
            var res = res;
            var item = res.currentTarget.dataset.item;
            wx.navigateTo({
              url: '../comment/comment?news_id=' + this.data.nid + "&comment_id=" + this.data.comment_id + "&is_private=" + this.data.isPrivate,
                complete: function() {

                }
            });
        },
        onItemClicked: function (e) {
            // wx.navigateTo({
            //     url: '../detail/detail?url={0}'.format(this.data.link),
            // });
        },
        onVoiceClicked: function (e) {
          this.triggerEvent('voiceClicked', { start: !this.data.audioRecording, id: this.data.comment_id, parentId: this.data.parentId, audioPath: this.data.audioPath});
            this.setData({
                audioRecording: !this.data.audioRecording
            });
        },

        onDeleteClicked: function (e) {
            util.showModalDialog('是否删除当前评论？', res => {
              if (res.confirm) {
                util.showLoadingDialog('数据提交中');
              dataManage.requestDataWithAPI(`comment/del/${this.data.comment_id}`, 'POST', {}, true, (res) =>{
                  evenBus.emit(constant.TAG_COMMENT_DELETED, { comment_id: this.data.comment_id});
                }, null, ()=>{
                    util.hideLoadingDialog();
                });
            }});
        },

        onVideoPlayed: function (e) {
            console.error(this.data.mediaVideoPath);
            wx.navigateTo({
              url: `/pages/videoplayer/videoplayer?url=${base64.encode(this.data.mediaVideoPath)}`
            })
        },

        _vote: function(cb) {
          dataManage.requestDataWithAPI("comment/event/" + this.data.comment_id, "POST", {}, true, (res)=> {
                if (res.code == 0) {
                    wx.showToast({
                        title: '谢谢点赞',
                    })
                    cb();
                }
            }, function(res) {
                wx.showToast({
                    title: res.message,
                    icon: "none"
                })
            }, function() {

            });
        },
    }

})