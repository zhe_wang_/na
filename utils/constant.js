/**
 * Created by zhaofei on 16-12-27.
 */

module.exports = {
    HTTP_METHODS: ['OPTIONS', 'GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'TRACE', 'CONNECT'],
    TAG_STOP_VOICE: 'stop_voice',
    TAG_ADDRESS_SELECTED: 'address_selected',
    TAG_REMARK_SELECTED: 'address_remark',
    TAG_COMMENT_DELETED: 'comment_delete',
    TAG_ORDER_REFUND: 'order_refund',
    TAG_NEWS_ADD_COLLECTION: 'news_add_collection',
    TAG_NEWS_LIKE: 'news_like',
    TAG_CREDIT_DETACHED: 'credit_detached'
}