/**
 * Created by staff on 2018/10/17.
 */

/**
 * 参考 Vue 的 lru 算法
 * 基本思路:
 * 基于一个双链表的数据结构，在没有满员的情况下，新来的 k-v 放在链表的尾部，以后每次获取缓存中的 k-v 时就将该k-v移到最末尾，缓存满的时候优先淘汰头部的。
 */
"use strict";
export default class LRUCache {
    /**
     * 利用集合_keymap来存储已有的节点，在判断是否存在时，直接读取属性就行，不用在遍历一遍链表，这样降低了在查找过程中的时间复杂度。
     * head代表着头节点，tail代表着尾节点，链表中的节点是这样的：
     *  node {
     *  value: 键值,
     *  key: 键名,
     *  older: 指向前一个节点，head的older指向undefined,
     *  newer: 指向下一个节点，tail的newer指向undefined
}
     * @param limit
     */
    constructor(limit) {
        // 标识当前缓存数组的大小
        this.size = 0;
        // 标识缓存数组能达到的最大长度
        this.limit = limit || 3;
        // head（最不常用的项），tail（最常用的项）全部初始化为undefined
        this.head = this.tail = undefined;

        this._keymap = Object.create(null);
    }

    /**
     *
     * @param key 键值
     * @param ifReturnNode  true则返回node  false则返回 node.value 默认 false
     * @returns {undefined}
     */
    get(key, ifReturnNode = false) {
        let node = this._keymap[key];
        if(node === undefined) {
            return undefined;
        }
        //如果查到的缓存对象已经是 tail
        if(node === this.tail) {
            return ifReturnNode ? node: node.value;
        }

        // HEAD--------------TAIL
        //   <.older   .newer>
        //  <--- add direction --
        //   A  B  C  <D>  E

        if(node.newer) {//处理 newer 指向
            if(node === this.head) {
                //如果查到的缓存对象是header(最近最少使用的)
                //则将 head指向原来 head的 newer 指向的缓存对象
                this.head = node.newer;
            }

            //将查找的对象的下一个node older 指向当前查找对象的 older
            node.newer.older = node.older; // C<-E
        }

        if(node.older) { //处理 older指向
            if(node === this.head) {
                node.older.newer = node.newer; //C->E;
            }
        }
        //处理查找到的对象的 newer 和 older
        node.newer = undefined;
        node.older = this.tail;
        if(this.tail) {
            this.tail.newer = node;
        }
        this.tail = node;

        return ifReturnNode ? node : node.value;

    }

    put(key, value) {
        let removed;
        let node = this.get(key);
        if(!node) {//如果当前没有对应的 key 则执行 add  否则是 refresh 类似于 map
            if(this.size >= this.limit) {
                removed = this.shift();

            }
            node = {key: key};
            this._keymap[key] = node;
            if(this.tail) {
                this.tail.newer = node;
                node.older = this.tail;
            } else {
                //如果缓存的长度为0，则将 head指向 node
                this.head = node;
            }
            node.value = value;
            this.tail = node;
            this.size++;
        }
        return removed;
    }

    /**
     * 删除head 并返回 所删除的 node
     * @returns {undefined|*}
     */
    shift() {
        let node = this.head;
        if(node) {
            //删除 head 并设置新的 head
            this.head = this.head.newer;
            this.head.older = undefined;
            node.newer = node.older = undefined;
            this._keymap[node.key] = undefined;
            //更新缓存数组长度
            this.size--;
        }
        return node;
    }
}