/**
 * Created by zhaofei on 16-12-27.
 */

// const HOST = "wchatapi.sibei.365jiab";
// const HOST = "wchatapi.365jia.cn";
const HOST = "news.ninedishes.com";
const HTTPS = "https://";
const URL_SPLITTER = "/";
const URL_API_BASE = HTTPS.concat(HOST, URL_SPLITTER, "api");
const API_AUTH = "/auth/login";
function makeAPIUrls(apiUrl) {
  if (apiUrl.startWith(URL_SPLITTER)) {
    return URL_API_BASE.concat(apiUrl);
  } else {
    return URL_API_BASE.concat(URL_SPLITTER, apiUrl);
  }
}
module.exports = {
  HTTPS: HTTPS,
  HOST: HOST,
  makeAPIUrls: makeAPIUrls,
  API_AUTH: API_AUTH
}
